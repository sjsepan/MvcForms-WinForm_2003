﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Ssepan.Application;

// Information about this assembly is defined by the following attributes. 
// Change them to the values specific to your project.

[assembly: AssemblyTitle("MvcForms.WinForm")]
[assembly: AssemblyDescription("Desktop GUI app demo, on Win, in C# / WinForms")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Free Software Foundation, Inc.")]
[assembly: AssemblyProduct("MvcForms.WinForm")]
[assembly: AssemblyCopyright("Copyright (C) 1989, 1991 Free Software Foundation, Inc.  \n59 Temple Place - Suite 330, Boston, MA  02111-1307, USA")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// The assembly version has the format "{Major}.{Minor}.{Build}.{Revision}".
// The form "{Major}.{Minor}.*" will automatically update the build and revision,
// and "{Major}.{Minor}.{Build}.*" will update just the revision.

[assembly: AssemblyVersion("0.8.0")]


namespace MvcForms.WinForm
{

    #region " Helper class to get information for the About form. "
    /// <summary>
    /// This class uses the System.Reflection.Assembly class to
    /// access assembly meta-data
    /// This class is ! a normal feature of AssemblyInfo.cs
    /// </summary>
    public class AssemblyInfo : AssemblyInfoBase
    {
       
        // Used by Helper Functions to access information from Assembly Attributes
        public AssemblyInfo()
        {
            base.myType = typeof(MvcForms.WinForm.MVCView);
            base.Website = "https://gitlab.com/sjsepan/MvcForms.WinForm";
            base.WebsiteLabel = "Gitlab";
            base.Designers = new String[] { "Stephen J Sepan", "Designer2", "Designer3" };
            base.Developers = new String[] { "Stephen J Sepan", "Developer2", "Developer3" };
            base.Documenters = new String[] { "Stephen J Sepan", "Documenter2", "Documenter3" };
            base.License = "./License.txt";
        }
    }
    #endregion
}
