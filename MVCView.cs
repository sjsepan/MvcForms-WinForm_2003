using System;
using System.Diagnostics;
using System.Drawing;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using Ssepan.Utility;
using Ssepan.Io;
using Ssepan.Application;
using Ssepan.Ui.WinForm;
using MvcLibrary;

namespace MvcForms.WinForm
{
	/// <summary>
	/// Summary description for MVCView.
	/// </summary>
	public class MVCView : System.Windows.Forms.Form
	{
		#region Declarations
		protected Boolean disposed;
		// private Boolean _ValueChangedProgrammatically;

		private const Int32 Button_Index_New = 0;
		private const Int32 Button_Index_Open = 1;
		private const Int32 Button_Index_Save = 2;
		private const Int32 Button_Index_Print = 3;
		private const Int32 Button_Index_Undo = 5;
		private const Int32 Button_Index_Redo = 6;
		private const Int32 Button_Index_Cut = 7;
		private const Int32 Button_Index_Copy = 8;
		private const Int32 Button_Index_Paste = 9;
		private const Int32 Button_Index_Delete = 10;
		private const Int32 Button_Index_Find = 11;
		private const Int32 Button_Index_Replace = 12;
		private const Int32 Button_Index_Refresh = 13;
		private const Int32 Button_Index_Preferences = 14;
		private const Int32 Button_Index_Properties = 15;
		private const Int32 Button_Index_Contents = 17;
//		private const Int32 Button_Index_About = 16;

		internal ListDictionary imageResources = null;

		//cancellation hook
		// System.Action cancelDelegate = null;
        

		protected MVCViewModel ViewModel = null;
		
		private System.Windows.Forms.Label lblSomeInt;
        private System.Windows.Forms.Label lblSomeString;
        private System.Windows.Forms.Label lblSomeBoolean;
        private System.Windows.Forms.TextBox txtSomeInt;
        private System.Windows.Forms.TextBox txtSomeString;
        private System.Windows.Forms.CheckBox chkSomeBoolean;
        private System.Windows.Forms.Button cmdRun;
        private System.Windows.Forms.Label lblSomeOtherInt;
        private System.Windows.Forms.Label lblSomeOtherString;
        private System.Windows.Forms.Label lblomeOtherBoolean;
        private System.Windows.Forms.TextBox txtSomeOtherInt;
        private System.Windows.Forms.TextBox txtSomeOtherString;
        private System.Windows.Forms.CheckBox chkSomeOtherBoolean;
        private System.Windows.Forms.Label lblStillAnotherInt;
        private System.Windows.Forms.Label lblStillAnotherString;
        private System.Windows.Forms.Label lblStillAnotherBoolean;
        private System.Windows.Forms.TextBox txtStillAnotherInt;
        private System.Windows.Forms.TextBox txtStillAnotherString;
        private System.Windows.Forms.CheckBox chkStillAnotherBoolean;
        private System.Windows.Forms.Button cmdFont;
        private System.Windows.Forms.Button cmdColor;
		private System.Windows.Forms.MainMenu menu;
		private System.Windows.Forms.MenuItem menuFile;
		private System.Windows.Forms.MenuItem menuFileNew;
		private System.Windows.Forms.MenuItem menuFileOpen;
		private System.Windows.Forms.MenuItem menuFileSave;
		private System.Windows.Forms.MenuItem menuFileSaveAs;
		private System.Windows.Forms.MenuItem menuFileSeparator1;
		private System.Windows.Forms.MenuItem menuFilePrint;
		private System.Windows.Forms.MenuItem menuFileSeparator2;
		private System.Windows.Forms.MenuItem menuFileExit;
		private System.Windows.Forms.MenuItem menuEdit;
		private System.Windows.Forms.MenuItem menuEditUndo;
		private System.Windows.Forms.MenuItem menuEditRedo;
		private System.Windows.Forms.MenuItem menuEditSeparator0;
		private System.Windows.Forms.MenuItem menuEditSelectAll;
		private System.Windows.Forms.MenuItem menuEditCut;
		private System.Windows.Forms.MenuItem menuEditCopy;
		private System.Windows.Forms.MenuItem menuEditPaste;
		private System.Windows.Forms.MenuItem menuEditDelete;
		private System.Windows.Forms.MenuItem menuEditSeparator1;
		private System.Windows.Forms.MenuItem menuEditFind;
		private System.Windows.Forms.MenuItem menuEditReplace;
		private System.Windows.Forms.MenuItem menuEditSeparator2;
		private System.Windows.Forms.MenuItem menuEditRefresh;
		private System.Windows.Forms.MenuItem menuEditSeparator3;
		private System.Windows.Forms.MenuItem menuEditProperties;
		private System.Windows.Forms.MenuItem menuEditPreferences;
		private System.Windows.Forms.MenuItem menuWindow;
		private System.Windows.Forms.MenuItem menuWindowNewWindow;
		private System.Windows.Forms.MenuItem menuWindowTile;
		private System.Windows.Forms.MenuItem menuWindowCascade;
		private System.Windows.Forms.MenuItem menuWindowArrangeAll;
		private System.Windows.Forms.MenuItem menuWindowSeparator1;
		private System.Windows.Forms.MenuItem menuWindowHide;
		private System.Windows.Forms.MenuItem menuWindowShow;
		private System.Windows.Forms.MenuItem menuHelp;
		private System.Windows.Forms.MenuItem menuHelpContents;
		private System.Windows.Forms.MenuItem menuHelpIndex;
		private System.Windows.Forms.MenuItem menuHelpOnlineHelp;
		private System.Windows.Forms.MenuItem menuHelpSeparator1;
		private System.Windows.Forms.MenuItem menuHelpLicenceInformation;
		private System.Windows.Forms.MenuItem menuHelpCheckForUpdates;
		private System.Windows.Forms.MenuItem menuHelpSeparator2;
		private System.Windows.Forms.MenuItem menuHelpAbout;
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.Panel statusBar;
		private System.Windows.Forms.ProgressBar progressBar;
		private System.Windows.Forms.Label statusMessage;
		private System.Windows.Forms.Label errorMessage;
		private System.Windows.Forms.PictureBox actionIcon;
		private System.Windows.Forms.PictureBox dirtyIcon;
		private System.Windows.Forms.ToolBar toolBar1;
		private System.Windows.Forms.ToolBarButton btnFileNew;
		private System.Windows.Forms.ToolBarButton btnFileOpen;
		private System.Windows.Forms.ToolBarButton btnFileSave;
		private System.Windows.Forms.ToolBarButton btnFilePrint;
		private System.Windows.Forms.ToolBarButton btnSeparator1;
		private System.Windows.Forms.ToolBarButton btnEditUndo;
		private System.Windows.Forms.ToolBarButton btnEditRedo;
		private System.Windows.Forms.ToolBarButton btnEditCut;
		private System.Windows.Forms.ToolBarButton btnEditCopy;
		private System.Windows.Forms.ToolBarButton btnEditPaste;
		private System.Windows.Forms.ToolBarButton btnEditDelete;
		private System.Windows.Forms.ToolBarButton btnEditFind;
		private System.Windows.Forms.ToolBarButton btnEditReplace;
		private System.Windows.Forms.ToolBarButton btnEditRefresh;
		private System.Windows.Forms.ToolBarButton btnEditPreferences;
		private System.Windows.Forms.ToolBarButton btnEditProperties;
		private System.Windows.Forms.ToolBarButton btnSeparator2;
		private System.Windows.Forms.ToolBarButton btnHelpContents;
		private System.ComponentModel.IContainer components;
		#endregion Declarations

		#region Constructors    
		public MVCView()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			
			statusMessage.Text = "";
			errorMessage.Text = "";

			////(re)define default output delegate
			//ConsoleApplication.defaultOutputDelegate = ConsoleApplication.messageBoxWrapperOutputDelegate;

			//subscribe to view's notifications
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged += new PropertyChangedEventHandler(PropertyChangedEventHandlerDelegate);
			}

			InitViewModel();

//			BindSizeAndLocation();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		#endregion Constructors

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(MVCView));
			this.lblSomeInt = new System.Windows.Forms.Label();
			this.lblSomeString = new System.Windows.Forms.Label();
			this.lblSomeBoolean = new System.Windows.Forms.Label();
			this.txtSomeInt = new System.Windows.Forms.TextBox();
			this.txtSomeString = new System.Windows.Forms.TextBox();
			this.chkSomeBoolean = new System.Windows.Forms.CheckBox();
			this.cmdRun = new System.Windows.Forms.Button();
			this.lblSomeOtherInt = new System.Windows.Forms.Label();
			this.lblSomeOtherString = new System.Windows.Forms.Label();
			this.lblomeOtherBoolean = new System.Windows.Forms.Label();
			this.txtSomeOtherInt = new System.Windows.Forms.TextBox();
			this.txtSomeOtherString = new System.Windows.Forms.TextBox();
			this.chkSomeOtherBoolean = new System.Windows.Forms.CheckBox();
			this.lblStillAnotherInt = new System.Windows.Forms.Label();
			this.lblStillAnotherString = new System.Windows.Forms.Label();
			this.lblStillAnotherBoolean = new System.Windows.Forms.Label();
			this.txtStillAnotherInt = new System.Windows.Forms.TextBox();
			this.txtStillAnotherString = new System.Windows.Forms.TextBox();
			this.chkStillAnotherBoolean = new System.Windows.Forms.CheckBox();
			this.cmdFont = new System.Windows.Forms.Button();
			this.cmdColor = new System.Windows.Forms.Button();
			this.menu = new System.Windows.Forms.MainMenu();
			this.menuFile = new System.Windows.Forms.MenuItem();
			this.menuFileNew = new System.Windows.Forms.MenuItem();
			this.menuFileOpen = new System.Windows.Forms.MenuItem();
			this.menuFileSave = new System.Windows.Forms.MenuItem();
			this.menuFileSaveAs = new System.Windows.Forms.MenuItem();
			this.menuFileSeparator1 = new System.Windows.Forms.MenuItem();
			this.menuFilePrint = new System.Windows.Forms.MenuItem();
			this.menuFileSeparator2 = new System.Windows.Forms.MenuItem();
			this.menuFileExit = new System.Windows.Forms.MenuItem();
			this.menuEdit = new System.Windows.Forms.MenuItem();
			this.menuEditUndo = new System.Windows.Forms.MenuItem();
			this.menuEditRedo = new System.Windows.Forms.MenuItem();
			this.menuEditSeparator0 = new System.Windows.Forms.MenuItem();
			this.menuEditSelectAll = new System.Windows.Forms.MenuItem();
			this.menuEditCut = new System.Windows.Forms.MenuItem();
			this.menuEditCopy = new System.Windows.Forms.MenuItem();
			this.menuEditPaste = new System.Windows.Forms.MenuItem();
			this.menuEditDelete = new System.Windows.Forms.MenuItem();
			this.menuEditSeparator1 = new System.Windows.Forms.MenuItem();
			this.menuEditFind = new System.Windows.Forms.MenuItem();
			this.menuEditReplace = new System.Windows.Forms.MenuItem();
			this.menuEditSeparator2 = new System.Windows.Forms.MenuItem();
			this.menuEditRefresh = new System.Windows.Forms.MenuItem();
			this.menuEditSeparator3 = new System.Windows.Forms.MenuItem();
			this.menuEditPreferences = new System.Windows.Forms.MenuItem();
			this.menuEditProperties = new System.Windows.Forms.MenuItem();
			this.menuWindow = new System.Windows.Forms.MenuItem();
			this.menuWindowNewWindow = new System.Windows.Forms.MenuItem();
			this.menuWindowTile = new System.Windows.Forms.MenuItem();
			this.menuWindowCascade = new System.Windows.Forms.MenuItem();
			this.menuWindowArrangeAll = new System.Windows.Forms.MenuItem();
			this.menuWindowSeparator1 = new System.Windows.Forms.MenuItem();
			this.menuWindowHide = new System.Windows.Forms.MenuItem();
			this.menuWindowShow = new System.Windows.Forms.MenuItem();
			this.menuHelp = new System.Windows.Forms.MenuItem();
			this.menuHelpContents = new System.Windows.Forms.MenuItem();
			this.menuHelpIndex = new System.Windows.Forms.MenuItem();
			this.menuHelpOnlineHelp = new System.Windows.Forms.MenuItem();
			this.menuHelpSeparator1 = new System.Windows.Forms.MenuItem();
			this.menuHelpLicenceInformation = new System.Windows.Forms.MenuItem();
			this.menuHelpCheckForUpdates = new System.Windows.Forms.MenuItem();
			this.menuHelpSeparator2 = new System.Windows.Forms.MenuItem();
			this.menuHelpAbout = new System.Windows.Forms.MenuItem();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.statusBar = new System.Windows.Forms.Panel();
			this.dirtyIcon = new System.Windows.Forms.PictureBox();
			this.actionIcon = new System.Windows.Forms.PictureBox();
			this.errorMessage = new System.Windows.Forms.Label();
			this.statusMessage = new System.Windows.Forms.Label();
			this.progressBar = new System.Windows.Forms.ProgressBar();
			this.toolBar1 = new System.Windows.Forms.ToolBar();
			this.btnFileNew = new System.Windows.Forms.ToolBarButton();
			this.btnFileOpen = new System.Windows.Forms.ToolBarButton();
			this.btnFileSave = new System.Windows.Forms.ToolBarButton();
			this.btnFilePrint = new System.Windows.Forms.ToolBarButton();
			this.btnSeparator1 = new System.Windows.Forms.ToolBarButton();
			this.btnEditUndo = new System.Windows.Forms.ToolBarButton();
			this.btnEditRedo = new System.Windows.Forms.ToolBarButton();
			this.btnEditCut = new System.Windows.Forms.ToolBarButton();
			this.btnEditCopy = new System.Windows.Forms.ToolBarButton();
			this.btnEditPaste = new System.Windows.Forms.ToolBarButton();
			this.btnEditDelete = new System.Windows.Forms.ToolBarButton();
			this.btnEditFind = new System.Windows.Forms.ToolBarButton();
			this.btnEditReplace = new System.Windows.Forms.ToolBarButton();
			this.btnEditRefresh = new System.Windows.Forms.ToolBarButton();
			this.btnEditPreferences = new System.Windows.Forms.ToolBarButton();
			this.btnEditProperties = new System.Windows.Forms.ToolBarButton();
			this.btnSeparator2 = new System.Windows.Forms.ToolBarButton();
			this.btnHelpContents = new System.Windows.Forms.ToolBarButton();
			this.statusBar.SuspendLayout();
			this.SuspendLayout();
			// 
			// lblSomeInt
			// 
			this.lblSomeInt.AutoSize = true;
			this.lblSomeInt.Location = new System.Drawing.Point(8, 32);
			this.lblSomeInt.Name = "lblSomeInt";
			this.lblSomeInt.Size = new System.Drawing.Size(20, 16);
			this.lblSomeInt.TabIndex = 119;
			this.lblSomeInt.Text = "Int:";
			// 
			// lblSomeString
			// 
			this.lblSomeString.AutoSize = true;
			this.lblSomeString.Location = new System.Drawing.Point(8, 56);
			this.lblSomeString.Name = "lblSomeString";
			this.lblSomeString.Size = new System.Drawing.Size(37, 16);
			this.lblSomeString.TabIndex = 120;
			this.lblSomeString.Text = "String:";
			// 
			// lblSomeBoolean
			// 
			this.lblSomeBoolean.AutoSize = true;
			this.lblSomeBoolean.Location = new System.Drawing.Point(8, 80);
			this.lblSomeBoolean.Name = "lblSomeBoolean";
			this.lblSomeBoolean.Size = new System.Drawing.Size(49, 16);
			this.lblSomeBoolean.TabIndex = 120;
			this.lblSomeBoolean.Text = "Boolean:";
			// 
			// txtSomeInt
			// 
			this.txtSomeInt.Location = new System.Drawing.Point(56, 32);
			this.txtSomeInt.Name = "txtSomeInt";
			this.txtSomeInt.TabIndex = 122;
			this.txtSomeInt.Text = "";
			// 
			// txtSomeString
			// 
			this.txtSomeString.Location = new System.Drawing.Point(56, 56);
			this.txtSomeString.Name = "txtSomeString";
			this.txtSomeString.TabIndex = 123;
			this.txtSomeString.Text = "";
			// 
			// chkSomeBoolean
			// 
			this.chkSomeBoolean.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.chkSomeBoolean.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.chkSomeBoolean.Location = new System.Drawing.Point(56, 80);
			this.chkSomeBoolean.Name = "chkSomeBoolean";
			this.chkSomeBoolean.Size = new System.Drawing.Size(17, 17);
			this.chkSomeBoolean.TabIndex = 124;
			this.chkSomeBoolean.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// cmdRun
			// 
			this.cmdRun.Location = new System.Drawing.Point(536, 96);
			this.cmdRun.Name = "cmdRun";
			this.cmdRun.TabIndex = 125;
			this.cmdRun.Text = "Run";
			this.cmdRun.Click += new System.EventHandler(this.cmdRun_Click);
			// 
			// lblSomeOtherInt
			// 
			this.lblSomeOtherInt.AutoSize = true;
			this.lblSomeOtherInt.Location = new System.Drawing.Point(168, 32);
			this.lblSomeOtherInt.Name = "lblSomeOtherInt";
			this.lblSomeOtherInt.Size = new System.Drawing.Size(51, 16);
			this.lblSomeOtherInt.TabIndex = 126;
			this.lblSomeOtherInt.Text = "Other Int:";
			// 
			// lblSomeOtherString
			// 
			this.lblSomeOtherString.AutoSize = true;
			this.lblSomeOtherString.Location = new System.Drawing.Point(168, 56);
			this.lblSomeOtherString.Name = "lblSomeOtherString";
			this.lblSomeOtherString.Size = new System.Drawing.Size(68, 16);
			this.lblSomeOtherString.TabIndex = 127;
			this.lblSomeOtherString.Text = "Other String:";
			// 
			// lblomeOtherBoolean
			// 
			this.lblomeOtherBoolean.AutoSize = true;
			this.lblomeOtherBoolean.Location = new System.Drawing.Point(168, 80);
			this.lblomeOtherBoolean.Name = "lblomeOtherBoolean";
			this.lblomeOtherBoolean.Size = new System.Drawing.Size(80, 16);
			this.lblomeOtherBoolean.TabIndex = 127;
			this.lblomeOtherBoolean.Text = "Other Boolean:";
			// 
			// txtSomeOtherInt
			// 
			this.txtSomeOtherInt.Location = new System.Drawing.Point(248, 32);
			this.txtSomeOtherInt.Name = "txtSomeOtherInt";
			this.txtSomeOtherInt.TabIndex = 128;
			this.txtSomeOtherInt.Text = "";
			// 
			// txtSomeOtherString
			// 
			this.txtSomeOtherString.Location = new System.Drawing.Point(248, 56);
			this.txtSomeOtherString.Name = "txtSomeOtherString";
			this.txtSomeOtherString.TabIndex = 129;
			this.txtSomeOtherString.Text = "";
			// 
			// chkSomeOtherBoolean
			// 
			this.chkSomeOtherBoolean.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.chkSomeOtherBoolean.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.chkSomeOtherBoolean.Location = new System.Drawing.Point(248, 80);
			this.chkSomeOtherBoolean.Name = "chkSomeOtherBoolean";
			this.chkSomeOtherBoolean.Size = new System.Drawing.Size(17, 17);
			this.chkSomeOtherBoolean.TabIndex = 130;
			this.chkSomeOtherBoolean.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblStillAnotherInt
			// 
			this.lblStillAnotherInt.AutoSize = true;
			this.lblStillAnotherInt.Location = new System.Drawing.Point(360, 32);
			this.lblStillAnotherInt.Name = "lblStillAnotherInt";
			this.lblStillAnotherInt.Size = new System.Drawing.Size(63, 16);
			this.lblStillAnotherInt.TabIndex = 131;
			this.lblStillAnotherInt.Text = "Another Int:";
			// 
			// lblStillAnotherString
			// 
			this.lblStillAnotherString.AutoSize = true;
			this.lblStillAnotherString.Location = new System.Drawing.Point(360, 56);
			this.lblStillAnotherString.Name = "lblStillAnotherString";
			this.lblStillAnotherString.Size = new System.Drawing.Size(80, 16);
			this.lblStillAnotherString.TabIndex = 132;
			this.lblStillAnotherString.Text = "Another String:";
			// 
			// lblStillAnotherBoolean
			// 
			this.lblStillAnotherBoolean.AutoSize = true;
			this.lblStillAnotherBoolean.Location = new System.Drawing.Point(360, 80);
			this.lblStillAnotherBoolean.Name = "lblStillAnotherBoolean";
			this.lblStillAnotherBoolean.Size = new System.Drawing.Size(92, 16);
			this.lblStillAnotherBoolean.TabIndex = 132;
			this.lblStillAnotherBoolean.Text = "Another Boolean:";
			// 
			// txtStillAnotherInt
			// 
			this.txtStillAnotherInt.Location = new System.Drawing.Point(448, 32);
			this.txtStillAnotherInt.Name = "txtStillAnotherInt";
			this.txtStillAnotherInt.TabIndex = 133;
			this.txtStillAnotherInt.Text = "";
			// 
			// txtStillAnotherString
			// 
			this.txtStillAnotherString.Location = new System.Drawing.Point(448, 56);
			this.txtStillAnotherString.Name = "txtStillAnotherString";
			this.txtStillAnotherString.TabIndex = 134;
			this.txtStillAnotherString.Text = "";
			// 
			// chkStillAnotherBoolean
			// 
			this.chkStillAnotherBoolean.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.chkStillAnotherBoolean.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.chkStillAnotherBoolean.Location = new System.Drawing.Point(456, 80);
			this.chkStillAnotherBoolean.Name = "chkStillAnotherBoolean";
			this.chkStillAnotherBoolean.Size = new System.Drawing.Size(14, 17);
			this.chkStillAnotherBoolean.TabIndex = 135;
			this.chkStillAnotherBoolean.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// cmdFont
			// 
			this.cmdFont.Location = new System.Drawing.Point(536, 168);
			this.cmdFont.Name = "cmdFont";
			this.cmdFont.TabIndex = 136;
			this.cmdFont.Text = "Font";
			this.cmdFont.Click += new System.EventHandler(this.cmdFont_Click);
			// 
			// cmdColor
			// 
			this.cmdColor.Location = new System.Drawing.Point(536, 136);
			this.cmdColor.Name = "cmdColor";
			this.cmdColor.TabIndex = 136;
			this.cmdColor.Text = "Color";
			this.cmdColor.Click += new System.EventHandler(this.cmdColor_Click);
			// 
			// menu
			// 
			this.menu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																				 this.menuFile,
																				 this.menuEdit,
																				 this.menuWindow,
																				 this.menuHelp});
			// 
			// menuFile
			// 
			this.menuFile.Index = 0;
			this.menuFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					 this.menuFileNew,
																					 this.menuFileOpen,
																					 this.menuFileSave,
																					 this.menuFileSaveAs,
																					 this.menuFileSeparator1,
																					 this.menuFilePrint,
																					 this.menuFileSeparator2,
																					 this.menuFileExit});
			this.menuFile.Text = "&File";
			// 
			// menuFileNew
			// 
			this.menuFileNew.Index = 0;
			this.menuFileNew.Shortcut = System.Windows.Forms.Shortcut.CtrlN;
			this.menuFileNew.Text = "&New";
			this.menuFileNew.Click += new System.EventHandler(this.MenuFileNew_Click);
			// 
			// menuFileOpen
			// 
			this.menuFileOpen.Index = 1;
			this.menuFileOpen.Shortcut = System.Windows.Forms.Shortcut.CtrlO;
			this.menuFileOpen.Text = "&Open";
			this.menuFileOpen.Click += new System.EventHandler(this.MenuFileOpen_Click);
			// 
			// menuFileSave
			// 
			this.menuFileSave.Index = 2;
			this.menuFileSave.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
			this.menuFileSave.Text = "&Save";
			this.menuFileSave.Click += new System.EventHandler(this.MenuFileSave_Click);
			// 
			// menuFileSaveAs
			// 
			this.menuFileSaveAs.Index = 3;
			this.menuFileSaveAs.Text = "Save &As...";
			this.menuFileSaveAs.Click += new System.EventHandler(this.MenuFileSaveAs_Click);
			// 
			// menuFileSeparator1
			// 
			this.menuFileSeparator1.Index = 4;
			this.menuFileSeparator1.Text = "-";
			// 
			// menuFilePrint
			// 
			this.menuFilePrint.Index = 5;
			this.menuFilePrint.Shortcut = System.Windows.Forms.Shortcut.CtrlP;
			this.menuFilePrint.Text = "&Print...";
			this.menuFilePrint.Click += new System.EventHandler(this.MenuFilePrint_Click);
			// 
			// menuFileSeparator2
			// 
			this.menuFileSeparator2.Index = 6;
			this.menuFileSeparator2.Text = "-";
			// 
			// menuFileExit
			// 
			this.menuFileExit.Index = 7;
			this.menuFileExit.Shortcut = System.Windows.Forms.Shortcut.CtrlQ;
			this.menuFileExit.Text = "E&xit";
			this.menuFileExit.Click += new System.EventHandler(this.MenuFileQuit_Click);
			// 
			// menuEdit
			// 
			this.menuEdit.Index = 1;
			this.menuEdit.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					 this.menuEditUndo,
																					 this.menuEditRedo,
																					 this.menuEditSeparator0,
																					 this.menuEditSelectAll,
																					 this.menuEditCut,
																					 this.menuEditCopy,
																					 this.menuEditPaste,
																					 this.menuEditDelete,
																					 this.menuEditSeparator1,
																					 this.menuEditFind,
																					 this.menuEditReplace,
																					 this.menuEditSeparator2,
																					 this.menuEditRefresh,
																					 this.menuEditSeparator3,
																					 this.menuEditPreferences,
																					 this.menuEditProperties});
			this.menuEdit.Text = "&Edit";
			// 
			// menuEditUndo
			// 
			this.menuEditUndo.Index = 0;
			this.menuEditUndo.Shortcut = System.Windows.Forms.Shortcut.CtrlZ;
			this.menuEditUndo.Text = "&Undo";
			this.menuEditUndo.Click += new System.EventHandler(this.MenuEditUndo_Click);
			// 
			// menuEditRedo
			// 
			this.menuEditRedo.Index = 1;
			this.menuEditRedo.Shortcut = System.Windows.Forms.Shortcut.CtrlY;
			this.menuEditRedo.Text = "&Redo";
			this.menuEditRedo.Click += new System.EventHandler(this.MenuEditRedo_Click);
			// 
			// menuEditSeparator0
			// 
			this.menuEditSeparator0.Index = 2;
			this.menuEditSeparator0.Text = "-";
			// 
			// menuEditSelectAll
			// 
			this.menuEditSelectAll.Index = 3;
			this.menuEditSelectAll.Shortcut = System.Windows.Forms.Shortcut.CtrlA;
			this.menuEditSelectAll.Text = "Select &All";
			this.menuEditSelectAll.Click += new System.EventHandler(this.MenuEditSelectAll_Click);
			// 
			// menuEditCut
			// 
			this.menuEditCut.Index = 4;
			this.menuEditCut.Shortcut = System.Windows.Forms.Shortcut.CtrlX;
			this.menuEditCut.Text = "Cu&t";
			this.menuEditCut.Click += new System.EventHandler(this.MenuEditCut_Click);
			// 
			// menuEditCopy
			// 
			this.menuEditCopy.Index = 5;
			this.menuEditCopy.Shortcut = System.Windows.Forms.Shortcut.CtrlC;
			this.menuEditCopy.Text = "&Copy";
			this.menuEditCopy.Click += new System.EventHandler(this.MenuEditCopy_Click);
			// 
			// menuEditPaste
			// 
			this.menuEditPaste.Index = 6;
			this.menuEditPaste.Shortcut = System.Windows.Forms.Shortcut.CtrlV;
			this.menuEditPaste.Text = "&Paste";
			this.menuEditPaste.Click += new System.EventHandler(this.MenuEditPaste_Click);
			// 
			// menuEditDelete
			// 
			this.menuEditDelete.Index = 7;
			this.menuEditDelete.Shortcut = System.Windows.Forms.Shortcut.Del;
			this.menuEditDelete.Text = "&Delete";
			this.menuEditDelete.Click += new System.EventHandler(this.MenuEditDelete_Click);
			// 
			// menuEditSeparator1
			// 
			this.menuEditSeparator1.Index = 8;
			this.menuEditSeparator1.Text = "-";
			// 
			// menuEditFind
			// 
			this.menuEditFind.Index = 9;
			this.menuEditFind.Shortcut = System.Windows.Forms.Shortcut.CtrlF;
			this.menuEditFind.Text = "&Find...";
			this.menuEditFind.Click += new System.EventHandler(this.MenuEditFind_Click);
			// 
			// menuEditReplace
			// 
			this.menuEditReplace.Index = 10;
			this.menuEditReplace.Shortcut = System.Windows.Forms.Shortcut.CtrlH;
			this.menuEditReplace.Text = "Rep&lace...";
			this.menuEditReplace.Click += new System.EventHandler(this.MenuEditReplace_Click);
			// 
			// menuEditSeparator2
			// 
			this.menuEditSeparator2.Index = 11;
			this.menuEditSeparator2.Text = "-";
			// 
			// menuEditRefresh
			// 
			this.menuEditRefresh.Index = 12;
			this.menuEditRefresh.Shortcut = System.Windows.Forms.Shortcut.F5;
			this.menuEditRefresh.Text = "R&efresh";
			this.menuEditRefresh.Click += new System.EventHandler(this.MenuEditRefresh_Click);
			// 
			// menuEditSeparator3
			// 
			this.menuEditSeparator3.Index = 13;
			this.menuEditSeparator3.Text = "-";
			// 
			// menuEditPreferences
			// 
			this.menuEditPreferences.Index = 14;
			this.menuEditPreferences.Text = "Prefere&nces...";
			this.menuEditPreferences.Click += new System.EventHandler(this.MenuEditPreferences_Click);
			// 
			// menuEditProperties
			// 
			this.menuEditProperties.Index = 15;
			this.menuEditProperties.Text = "Pr&operties...";
			this.menuEditProperties.Click += new System.EventHandler(this.MenuEditProperties_Click);
			// 
			// menuWindow
			// 
			this.menuWindow.Index = 2;
			this.menuWindow.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					   this.menuWindowNewWindow,
																					   this.menuWindowTile,
																					   this.menuWindowCascade,
																					   this.menuWindowArrangeAll,
																					   this.menuWindowSeparator1,
																					   this.menuWindowHide,
																					   this.menuWindowShow});
			this.menuWindow.Text = "&Window";
			// 
			// menuWindowNewWindow
			// 
			this.menuWindowNewWindow.Index = 0;
			this.menuWindowNewWindow.Text = "&New Window";
			this.menuWindowNewWindow.Click += new System.EventHandler(this.MenuWindowNewWindow_Click);
			// 
			// menuWindowTile
			// 
			this.menuWindowTile.Index = 1;
			this.menuWindowTile.Text = "&Tile";
			this.menuWindowTile.Click += new System.EventHandler(this.MenuWindowTile_Click);
			// 
			// menuWindowCascade
			// 
			this.menuWindowCascade.Index = 2;
			this.menuWindowCascade.Text = "&Cascade";
			this.menuWindowCascade.Click += new System.EventHandler(this.MenuWindowCascade_Click);
			// 
			// menuWindowArrangeAll
			// 
			this.menuWindowArrangeAll.Index = 3;
			this.menuWindowArrangeAll.Text = "&Arrange All";
			this.menuWindowArrangeAll.Click += new System.EventHandler(this.MenuWindowArrangeAll_Click);
			// 
			// menuWindowSeparator1
			// 
			this.menuWindowSeparator1.Index = 4;
			this.menuWindowSeparator1.Text = "-";
			// 
			// menuWindowHide
			// 
			this.menuWindowHide.Index = 5;
			this.menuWindowHide.Text = "&Hide";
			this.menuWindowHide.Click += new System.EventHandler(this.MenuWindowHide_Click);
			// 
			// menuWindowShow
			// 
			this.menuWindowShow.Index = 6;
			this.menuWindowShow.Text = "&Show";
			this.menuWindowShow.Click += new System.EventHandler(this.MenuWindowShow_Click);
			// 
			// menuHelp
			// 
			this.menuHelp.Index = 3;
			this.menuHelp.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					 this.menuHelpContents,
																					 this.menuHelpIndex,
																					 this.menuHelpOnlineHelp,
																					 this.menuHelpSeparator1,
																					 this.menuHelpLicenceInformation,
																					 this.menuHelpCheckForUpdates,
																					 this.menuHelpSeparator2,
																					 this.menuHelpAbout});
			this.menuHelp.Text = "&Help";
			// 
			// menuHelpContents
			// 
			this.menuHelpContents.Index = 0;
			this.menuHelpContents.Shortcut = System.Windows.Forms.Shortcut.F1;
			this.menuHelpContents.Text = "&Contents";
			this.menuHelpContents.Click += new System.EventHandler(this.MenuHelpContents_Click);
			// 
			// menuHelpIndex
			// 
			this.menuHelpIndex.Index = 1;
			this.menuHelpIndex.Text = "&Index";
			this.menuHelpIndex.Click += new System.EventHandler(this.MenuHelpIndex_Click);
			// 
			// menuHelpOnlineHelp
			// 
			this.menuHelpOnlineHelp.Index = 2;
			this.menuHelpOnlineHelp.Text = "&Online Help";
			this.menuHelpOnlineHelp.Click += new System.EventHandler(this.MenuHelpOnlineHelp_Click);
			// 
			// menuHelpSeparator1
			// 
			this.menuHelpSeparator1.Index = 3;
			this.menuHelpSeparator1.Text = "-";
			// 
			// menuHelpLicenceInformation
			// 
			this.menuHelpLicenceInformation.Index = 4;
			this.menuHelpLicenceInformation.Text = "&Licence Information";
			this.menuHelpLicenceInformation.Click += new System.EventHandler(this.MenuHelpLicenceInformation_Click);
			// 
			// menuHelpCheckForUpdates
			// 
			this.menuHelpCheckForUpdates.Index = 5;
			this.menuHelpCheckForUpdates.Text = "Check for &Updates";
			this.menuHelpCheckForUpdates.Click += new System.EventHandler(this.MenuHelpCheckForUpdates_Click);
			// 
			// menuHelpSeparator2
			// 
			this.menuHelpSeparator2.Index = 6;
			this.menuHelpSeparator2.Text = "-";
			// 
			// menuHelpAbout
			// 
			this.menuHelpAbout.Index = 7;
			this.menuHelpAbout.Text = "&About MvcForms.WinForm ...";
			this.menuHelpAbout.Click += new System.EventHandler(this.MenuHelpAbout_Click);
			// 
			// imageList1
			// 
			this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
			// 
			// statusBar
			// 
			this.statusBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.statusBar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.statusBar.Controls.Add(this.dirtyIcon);
			this.statusBar.Controls.Add(this.actionIcon);
			this.statusBar.Controls.Add(this.errorMessage);
			this.statusBar.Controls.Add(this.statusMessage);
			this.statusBar.Controls.Add(this.progressBar);
			this.statusBar.Location = new System.Drawing.Point(0, 432);
			this.statusBar.Name = "statusBar";
			this.statusBar.Size = new System.Drawing.Size(640, 24);
			this.statusBar.TabIndex = 140;
			// 
			// dirtyIcon
			// 
			this.dirtyIcon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.dirtyIcon.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.dirtyIcon.Image = ((System.Drawing.Image)(resources.GetObject("dirtyIcon.Image")));
			this.dirtyIcon.Location = new System.Drawing.Point(600, 0);
			this.dirtyIcon.Name = "dirtyIcon";
			this.dirtyIcon.Size = new System.Drawing.Size(22, 22);
			this.dirtyIcon.TabIndex = 145;
			this.dirtyIcon.TabStop = false;
			this.dirtyIcon.Visible = false;
			// 
			// actionIcon
			// 
			this.actionIcon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.actionIcon.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.actionIcon.Location = new System.Drawing.Point(576, 0);
			this.actionIcon.Name = "actionIcon";
			this.actionIcon.Size = new System.Drawing.Size(22, 22);
			this.actionIcon.TabIndex = 144;
			this.actionIcon.TabStop = false;
			this.actionIcon.Visible = false;
			// 
			// errorMessage
			// 
			this.errorMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.errorMessage.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.errorMessage.ForeColor = System.Drawing.Color.Red;
			this.errorMessage.Location = new System.Drawing.Point(256, 0);
			this.errorMessage.Name = "errorMessage";
			this.errorMessage.Size = new System.Drawing.Size(218, 23);
			this.errorMessage.TabIndex = 143;
			this.errorMessage.Text = "Error";
			// 
			// statusMessage
			// 
			this.statusMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.statusMessage.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.statusMessage.ForeColor = System.Drawing.Color.Green;
			this.statusMessage.Location = new System.Drawing.Point(0, 0);
			this.statusMessage.Name = "statusMessage";
			this.statusMessage.Size = new System.Drawing.Size(256, 23);
			this.statusMessage.TabIndex = 142;
			this.statusMessage.Text = "Status";
			// 
			// progressBar
			// 
			this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.progressBar.Location = new System.Drawing.Point(472, 0);
			this.progressBar.Name = "progressBar";
			this.progressBar.TabIndex = 139;
			this.progressBar.Visible = false;
			// 
			// toolBar1
			// 
			this.toolBar1.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
																						this.btnFileNew,
																						this.btnFileOpen,
																						this.btnFileSave,
																						this.btnFilePrint,
																						this.btnSeparator1,
																						this.btnEditUndo,
																						this.btnEditRedo,
																						this.btnEditCut,
																						this.btnEditCopy,
																						this.btnEditPaste,
																						this.btnEditDelete,
																						this.btnEditFind,
																						this.btnEditReplace,
																						this.btnEditRefresh,
																						this.btnEditPreferences,
																						this.btnEditProperties,
																						this.btnSeparator2,
																						this.btnHelpContents});
			this.toolBar1.DropDownArrows = true;
			this.toolBar1.ImageList = this.imageList1;
			this.toolBar1.Location = new System.Drawing.Point(0, 0);
			this.toolBar1.Name = "toolBar1";
			this.toolBar1.ShowToolTips = true;
			this.toolBar1.Size = new System.Drawing.Size(632, 28);
			this.toolBar1.TabIndex = 141;
			this.toolBar1.ButtonClick += new System.Windows.Forms.ToolBarButtonClickEventHandler(this.toolBar1_ButtonClick);
			// 
			// btnFileNew
			// 
			this.btnFileNew.ImageIndex = 1;
			this.btnFileNew.ToolTipText = "New";
			// 
			// btnFileOpen
			// 
			this.btnFileOpen.ImageIndex = 2;
			this.btnFileOpen.ToolTipText = "Open";
			// 
			// btnFileSave
			// 
			this.btnFileSave.ImageIndex = 3;
			this.btnFileSave.ToolTipText = "Save";
			// 
			// btnFilePrint
			// 
			this.btnFilePrint.ImageIndex = 4;
			this.btnFilePrint.ToolTipText = "Print";
			// 
			// btnSeparator1
			// 
			this.btnSeparator1.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// btnEditUndo
			// 
			this.btnEditUndo.ImageIndex = 5;
			this.btnEditUndo.ToolTipText = "Undo";
			// 
			// btnEditRedo
			// 
			this.btnEditRedo.ImageIndex = 6;
			this.btnEditRedo.ToolTipText = "Redo";
			// 
			// btnEditCut
			// 
			this.btnEditCut.ImageIndex = 7;
			this.btnEditCut.ToolTipText = "Cut";
			// 
			// btnEditCopy
			// 
			this.btnEditCopy.ImageIndex = 8;
			this.btnEditCopy.ToolTipText = "Copy";
			// 
			// btnEditPaste
			// 
			this.btnEditPaste.ImageIndex = 9;
			this.btnEditPaste.ToolTipText = "Paste";
			// 
			// btnEditDelete
			// 
			this.btnEditDelete.ImageIndex = 10;
			this.btnEditDelete.ToolTipText = "Delete";
			// 
			// btnEditFind
			// 
			this.btnEditFind.ImageIndex = 11;
			this.btnEditFind.ToolTipText = "Find";
			// 
			// btnEditReplace
			// 
			this.btnEditReplace.ImageIndex = 12;
			this.btnEditReplace.ToolTipText = "Replace";
			// 
			// btnEditRefresh
			// 
			this.btnEditRefresh.ImageIndex = 13;
			this.btnEditRefresh.ToolTipText = "Refresh";
			// 
			// btnEditPreferences
			// 
			this.btnEditPreferences.ImageIndex = 14;
			this.btnEditPreferences.ToolTipText = "Preferences";
			// 
			// btnEditProperties
			// 
			this.btnEditProperties.ImageIndex = 15;
			this.btnEditProperties.ToolTipText = "Properties";
			// 
			// btnSeparator2
			// 
			this.btnSeparator2.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// btnHelpContents
			// 
			this.btnHelpContents.ImageIndex = 16;
			this.btnHelpContents.ToolTipText = "Help";
			// 
			// MVCView
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(632, 453);
			this.Controls.Add(this.toolBar1);
			this.Controls.Add(this.statusBar);
			this.Controls.Add(this.chkStillAnotherBoolean);
			this.Controls.Add(this.txtStillAnotherString);
			this.Controls.Add(this.lblSomeBoolean);
			this.Controls.Add(this.lblStillAnotherString);
			this.Controls.Add(this.lblStillAnotherInt);
			this.Controls.Add(this.lblomeOtherBoolean);
			this.Controls.Add(this.lblSomeOtherString);
			this.Controls.Add(this.lblSomeOtherInt);
			this.Controls.Add(this.lblStillAnotherBoolean);
			this.Controls.Add(this.lblSomeString);
			this.Controls.Add(this.lblSomeInt);
			this.Controls.Add(this.txtStillAnotherInt);
			this.Controls.Add(this.txtSomeOtherString);
			this.Controls.Add(this.txtSomeOtherInt);
			this.Controls.Add(this.txtSomeString);
			this.Controls.Add(this.txtSomeInt);
			this.Controls.Add(this.chkSomeOtherBoolean);
			this.Controls.Add(this.cmdRun);
			this.Controls.Add(this.chkSomeBoolean);
			this.Controls.Add(this.cmdFont);
			this.Controls.Add(this.cmdColor);
			this.Menu = this.menu;
			this.MinimumSize = new System.Drawing.Size(640, 480);
			this.Name = "MVCView";
			this.Text = "MVCView";
			this.Load += new EventHandler(View_Load);
			this.Closing += new CancelEventHandler(View_Closing);
			this.statusBar.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		#region INotifyPropertyChanged
		public event PropertyChangedEventHandler PropertyChanged;
		protected void OnPropertyChanged(String propertyName)
		{
			try
			{
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
				}
			}
			catch (Exception ex)
			{
				ViewModel.ErrorMessage = ex.Message;
				Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

				throw;
			}
		}
		#endregion INotifyPropertyChanged

		#region Properties
		private String _ViewName = ProgramBase.APP_NAME;
		public String ViewName
		{
			get { return _ViewName; }
			set
			{
				_ViewName = value;
				OnPropertyChanged("ViewName");
			}
		}
		#endregion Properties

		#region Events

		#region PropertyChangedEventHandlerDelegates
		/// <summary>
		/// Note: model property changes update UI manually.
		/// Note: handle settings property changes manually.
		/// Note: because settings properties are a subset of the model 
		///  (every settings property should be in the model, 
		///  but not every model property is persisted to settings)
		///  it is decided that for now the settings handler will 
		///  invoke the model handler as well.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void PropertyChangedEventHandlerDelegate(Object sender, PropertyChangedEventArgs e)
		{
			try
			{
				#region Model
				if (e.PropertyName == "IsChanged")
				{
					//ConsoleApplication.defaultOutputDelegate(String.Format("{0}", e.PropertyName));
					ApplySettings();
				}
				//Status Bar
				else if (e.PropertyName == "StatusMessage")
				{
					//replace default action by setting control property
					//skip status message updates after Viewmodel is null
					statusMessage.Text = (ViewModel != null ? ViewModel.StatusMessage : (String)null);
					//e = new PropertyChangedEventArgs(e.PropertyName + ".handled");

					//ConsoleApplication.defaultOutputDelegate(String.Format("{0}", StatusMessage));
				}
				else if (e.PropertyName == "ErrorMessage")
				{
					//replace default action by setting control property
					//skip status message updates after Viewmodel is null
					errorMessage.Text = (ViewModel != null ? ViewModel.ErrorMessage : (String)null);
					//e = new PropertyChangedEventArgs(e.PropertyName + ".handled");

					//ConsoleApplication.defaultOutputDelegate(String.Format("{0}", ErrorMessage));
				}
				else if (e.PropertyName == "CustomMessage")
				{
					//replace default action by setting control property
					//StatusBarCustomMessage.Text = ViewModel.CustomMessage;
					//e = new PropertyChangedEventArgs(e.PropertyName + ".handled");

					//ConsoleApplication.defaultOutputDelegate(String.Format("{0}", ErrorMessage));
				}
				else if (e.PropertyName == "ErrorMessageToolTipText")
				{
//					errorMessage.ToolTipText = ViewModel.ErrorMessageToolTipText;
				}
				else if (e.PropertyName == "ProgressBarValue")
				{
					progressBar.Value = ViewModel.ProgressBarValue;
				}
				else if (e.PropertyName == "ProgressBarMaximum")
				{
					progressBar.Maximum = ViewModel.ProgressBarMaximum;
				}
				else if (e.PropertyName == "ProgressBarMinimum")
				{
					progressBar.Minimum = ViewModel.ProgressBarMinimum;
				}
				else if (e.PropertyName == "ProgressBarStep")
				{
					// progressBar.PulseStep = ViewModel.ProgressBarStep;
				}
				else if (e.PropertyName == "ProgressBarIsMarquee")
				{
//					progressBar.Style = (ViewModel.ProgressBarIsMarquee ? ProgressBarStyle.Marquee : ProgressBarStyle.Blocks);//true;
				}
				else if (e.PropertyName == "ProgressBarIsVisible")
				{
					progressBar.Visible = (ViewModel.ProgressBarIsVisible);
				}
				else if (e.PropertyName == "ActionIconIsVisible")
				{
					actionIcon.Visible = (ViewModel.ActionIconIsVisible);
				}
				else if (e.PropertyName == "ActionIconImage")
				{
					actionIcon.Image = (ViewModel != null ? ViewModel.ActionIconImage : null);
				}
				else if (e.PropertyName == "DirtyIconIsVisible")
				{
					dirtyIcon.Visible = (ViewModel.DirtyIconIsVisible);
				}
				else if (e.PropertyName == "DirtyIconImage")
				{
					dirtyIcon.Image = ViewModel.DirtyIconImage;
				}
					//use if properties cannot be databound
				else if (e.PropertyName == "SomeInt")
				{
					txtSomeInt.Text = ModelController.Model.SomeInt.ToString();
				}
				else if (e.PropertyName == "SomeBoolean")
				{
					chkSomeBoolean.Checked = ModelController.Model.SomeBoolean;
				}
				else if (e.PropertyName == "SomeString")
				{
					txtSomeString.Text = ModelController.Model.SomeString;
				}
				else if (e.PropertyName == "StillAnotherInt")
				{
					txtStillAnotherInt.Text = ModelController.Model.StillAnotherComponent.StillAnotherInt.ToString();
				}
				else if (e.PropertyName == "StillAnotherBoolean")
				{
					chkStillAnotherBoolean.Checked = ModelController.Model.StillAnotherComponent.StillAnotherBoolean;
				}
				else if (e.PropertyName == "StillAnotherString")
				{
					txtStillAnotherString.Text = ModelController.Model.StillAnotherComponent.StillAnotherString;
				}
				else if (e.PropertyName == "SomeOtherInt")
				{
					txtSomeOtherInt.Text = ModelController.Model.SomeComponent.SomeOtherInt.ToString();
				}
				else if (e.PropertyName == "SomeOtherBoolean")
				{
					chkSomeOtherBoolean.Checked = ModelController.Model.SomeComponent.SomeOtherBoolean;
				}
				else if (e.PropertyName == "SomeOtherString")
				{
					txtSomeOtherString.Text = ModelController.Model.SomeComponent.SomeOtherString;
				}
					//else if (e.PropertyName == "SomeComponent")
					//{
					//    ConsoleApplication.defaultOutputDelegate(String.Format("SomeComponent: {0},{1},{2}", ModelController.Model.SomeComponent.SomeOtherInt, ModelController.Model.SomeComponent.SomeOtherBoolean, ModelController.Model.SomeComponent.SomeOtherString));
					//}
					//else if (e.PropertyName == "StillAnotherComponent")
					//{
					//    ConsoleApplication.defaultOutputDelegate(String.Format("StillAnotherComponent: {0},{1},{2}", ModelController.Model.StillAnotherComponent.StillAnotherInt, ModelController.Model.StillAnotherComponent.StillAnotherBoolean, ModelController.Model.StillAnotherComponent.StillAnotherString));
					//}
				else
				{
#if DEBUG_MODEL_PROPERTYCHANGED
                    ConsoleApplication.defaultOutputDelegate(String.Format("e.PropertyName: {0}", e.PropertyName));
#endif
				}
				#endregion Model

				#region Settings
				if (e.PropertyName == "Dirty")
				{
					//apply settings that don't have databindings
					ViewModel.DirtyIconIsVisible = (SettingsController.Settings.Dirty);
				}
				else
				{
#if DEBUG_SETTINGS_PROPERTYCHANGED
                    ConsoleApplication.defaultOutputDelegate(String.Format("e.PropertyName: {0}", e.PropertyName));
#endif
				}
				#endregion Settings
			}
			catch (Exception ex)
			{
				Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
			}
		}
		#endregion PropertyChangedEventHandlerDelegates

		#region Form Events
		private void View_Load(Object sender, EventArgs e)
		{
			try
			{
				ViewModel.StatusMessage = String.Format("{0} starting...", ViewName);

				ViewModel.StatusMessage = String.Format("{0} started.", ViewName);

				//_Run();//only called here in console apps; use button in forms apps
			}
			catch (Exception ex)
			{
				ViewModel.ErrorMessage = ex.Message;
				ViewModel.StatusMessage = String.Empty;

				Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
			}
		}

		private void View_Closing(Object sender, CancelEventArgs e)
		{
			Boolean resultDontQuit = false;

			try
			{
				ViewModel.FileExit(ref resultDontQuit);

				e.Cancel = resultDontQuit;

				if (!resultDontQuit)
				{
					//clean up data model here
					ViewModel.StatusMessage = String.Format("{0} completing...", ViewName);
					DisposeSettings();
					ViewModel.StatusMessage = String.Format("{0} completed.", ViewName);

					ViewModel = null;

					//Application.Instance.Quit();//this will only re-trigger the Closing event; just allow window to close
				}
			}
			catch (Exception ex)
			{
				Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

				if (ViewModel != null)
				{
					ViewModel.ErrorMessage = ex.Message;
				}
			}
		}
		#endregion Form Events

		#region Control Events
		private void txtSomeInt_TextChanged(Object sender, EventArgs e)
		{
			Int32 result = 0;

			try
			{
				result = Int32.Parse(txtSomeInt.Text);
			}
			catch (ArgumentNullException an_ex)
			{
				Log.Write(an_ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
			}
			catch (FormatException f_ex)
			{
				Log.Write(f_ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
			}
			catch (OverflowException o_ex)
			{
				Log.Write(o_ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
			}
			finally
			{
				ModelController.Model.SomeInt = result;
			}
		}

		private void txtSomeOtherInt_TextChanged(Object sender, EventArgs e)
		{
			Int32 result = 0;
            
			try
			{
				result = Int32.Parse(txtSomeOtherInt.Text);
			}
			catch (ArgumentNullException an_ex)
			{
				Log.Write(an_ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
			}
			catch (FormatException f_ex)
			{
				Log.Write(f_ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
			}
			catch (OverflowException o_ex)
			{
				Log.Write(o_ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
			}
			finally
			{
				ModelController.Model.SomeComponent.SomeOtherInt = result;
			}
		}

		private void txtStillAnotherInt_TextChanged(Object sender, EventArgs e)
		{
			Int32 result = 0;
            
			try
			{
				result = Int32.Parse(txtStillAnotherInt.Text);
			}
			catch (ArgumentNullException an_ex)
			{
				Log.Write(an_ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
			}
			catch (FormatException f_ex)
			{
				Log.Write(f_ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
			}
			catch (OverflowException o_ex)
			{
				Log.Write(o_ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
			}
			finally
			{
				ModelController.Model.StillAnotherComponent.StillAnotherInt = result;
			}
		}

		private void txtSomeString_TextChanged(Object sender, EventArgs e)
		{
			ModelController.Model.SomeString = txtSomeString.Text;
		}

		private void txtSomeOtherString_TextChanged(Object sender, EventArgs e)
		{
			ModelController.Model.SomeComponent.SomeOtherString = txtSomeOtherString.Text;
		}

		private void txtStillAnotherString_TextChanged(Object sender, EventArgs e)
		{
			ModelController.Model.StillAnotherComponent.StillAnotherString = txtStillAnotherString.Text;
		}

		private void chkSomeBoolean_CheckChanged(Object sender, EventArgs e)
		{
			ModelController.Model.SomeBoolean = chkSomeBoolean.Checked;
		}

		private void chkSomeOtherBoolean_CheckChanged(Object sender, EventArgs e)
		{
			ModelController.Model.SomeComponent.SomeOtherBoolean = chkSomeOtherBoolean.Checked;
		}

		private void chkStillAnotherBoolean_CheckChanged(Object sender, EventArgs e)
		{
			ModelController.Model.StillAnotherComponent.StillAnotherBoolean = chkStillAnotherBoolean.Checked;
		}
		private void cmdRun_Click(Object sender, EventArgs e)
		{
			//do something
			ViewModel.DoSomething();
		}
		private void cmdColor_Click(object sender, EventArgs e)
		{
			ViewModel.GetColor();
		}
		private void cmdFont_Click(object sender, EventArgs e)
		{
			ViewModel.GetFont();
		}
		#endregion Control Events

		#region Menu Events
		private void MenuFileNew_Click(object sender, EventArgs e)
		{
			ViewModel.FileNew();
		}
		private void MenuFileOpen_Click(object sender, EventArgs e)
		{
			ViewModel.FileOpen();
		}
		private void MenuFileSave_Click(object sender, EventArgs e)
		{
			ViewModel.FileSave();
		}
		private void MenuFileSaveAs_Click(object sender, EventArgs e)
		{
			ViewModel.FileSaveAs();
		}
		private void MenuFilePrint_Click(object sender, EventArgs e)
		{
			ViewModel.FilePrint();
		}
		private void MenuFilePrintPreview_Click(object sender, EventArgs e)
		{
			ViewModel.FilePrintPreview();
		}
		private void MenuFileQuit_Click(object sender, EventArgs e)
		{
			this.Close();
		}
		private void MenuEditUndo_Click(object sender, EventArgs e)
		{
			ViewModel.EditUndo();
		}
		private void MenuEditRedo_Click(object sender, EventArgs e)
		{
			ViewModel.EditRedo();
		}
		private void MenuEditSelectAll_Click(object sender, EventArgs e)
		{
			ViewModel.EditSelectAll();
		}
		private void MenuEditCut_Click(object sender, EventArgs e)
		{
			ViewModel.EditCut();
		}
		private void MenuEditCopy_Click(object sender, EventArgs e)
		{
			ViewModel.EditCopy();
		}
		private void MenuEditPaste_Click(object sender, EventArgs e)
		{
			ViewModel.EditPaste();
		}
		private void MenuEditPasteSpecial_Click(object sender, EventArgs e)
		{
			ViewModel.EditPasteSpecial();
		}
		private void MenuEditDelete_Click(object sender, EventArgs e)
		{
			ViewModel.EditDelete();
		}
		private void MenuEditFind_Click(object sender, EventArgs e)
		{
			ViewModel.EditFind();
		}
		private void MenuEditReplace_Click(object sender, EventArgs e)
		{
			ViewModel.EditReplace();
		}
		private void MenuEditRefresh_Click(object sender, EventArgs e)
		{
			ViewModel.EditRefresh();
		}
		private void MenuEditPreferences_Click(object sender, EventArgs e)
		{
			ViewModel.EditPreferences();
		}
		private void MenuEditProperties_Click(object sender, EventArgs e)
		{
			ViewModel.EditProperties();
		}
		private void MenuWindowNewWindow_Click(object sender, EventArgs e)
		{
			ViewModel.WindowNewWindow();
		}
		private void MenuWindowTile_Click(object sender, EventArgs e)
		{
			ViewModel.WindowTile();
		}
		private void MenuWindowCascade_Click(object sender, EventArgs e)
		{
			ViewModel.WindowCascade();
		}
		private void MenuWindowArrangeAll_Click(object sender, EventArgs e)
		{
			ViewModel.WindowArrangeAll();
		}
		private void MenuWindowHide_Click(object sender, EventArgs e)
		{
			ViewModel.WindowHide();
		}
		private void MenuWindowShow_Click(object sender, EventArgs e)
		{
			ViewModel.WindowShow();
		}
		private void MenuHelpContents_Click(object sender, EventArgs e)
		{
			ViewModel.HelpContents();
		}
		private void MenuHelpIndex_Click(object sender, EventArgs e)
		{
			ViewModel.HelpIndex();
		}
		private void MenuHelpOnlineHelp_Click(object sender, EventArgs e)
		{
			ViewModel.HelpOnlineHelp();
		}
		private void MenuHelpLicenceInformation_Click(object sender, EventArgs e)
		{
			ViewModel.HelpLicenceInformation();
		}
		private void MenuHelpCheckForUpdates_Click(object sender, EventArgs e)
		{
			ViewModel.HelpCheckForUpdates();
		}
		private void MenuHelpAbout_Click(object sender, EventArgs e)
		{
			ViewModel.HelpAbout();
		}
		private void toolBar1_ButtonClick(Object sender, ToolBarButtonClickEventArgs e)
		{
			// Evaluate the Button property to determine which button was clicked.
			switch(toolBar1.Buttons.IndexOf(e.Button))
			{
				case Button_Index_New:
					MenuFileNew_Click(sender, e);
					break;
				case Button_Index_Open:
					MenuFileOpen_Click(sender, e);
					break;
				case Button_Index_Save:
					MenuFileSave_Click(sender, e);
					break;
				case Button_Index_Print:
					MenuFilePrint_Click(sender, e);
					break;
				case Button_Index_Undo:
					MenuEditUndo_Click(sender, e);
					break;
				case Button_Index_Redo:
					MenuEditRedo_Click(sender, e);
					break;
				case Button_Index_Cut:
					MenuEditCut_Click(sender, e);
					break;
				case Button_Index_Copy:
					MenuEditCopy_Click(sender, e);
					break;
				case Button_Index_Paste:
					MenuEditPaste_Click(sender, e);
					break;
				case Button_Index_Delete:
					MenuEditDelete_Click(sender, e);
					break;
				case Button_Index_Find:
					MenuEditFind_Click(sender, e);
					break;
				case Button_Index_Replace:
					MenuEditReplace_Click(sender, e);
					break;
				case Button_Index_Refresh:
					MenuEditRefresh_Click(sender, e);
					break;
				case Button_Index_Preferences:
					MenuEditPreferences_Click(sender, e);
					break;
				case Button_Index_Properties:
					MenuEditProperties_Click(sender, e);
					break;
				case Button_Index_Contents:
					MenuHelpContents_Click(sender, e);
					break;

				// case 0:
				// 	break; 
				// case 1:
				// 	break; 
				// case 2:
				// 	break; 
			}
		}		
		#endregion Menu Events
		#endregion Events

        #region Methods
        #region FormAppBase
        protected void InitViewModel()
        {
            FileDialogInfo settingsFileDialogInfo = null;
            ListDictionary actionIconImages = null;
            
            try
            {
                //tell controller how model should notify view about non-persisted properties AND including model properties that may be part of settings
                ModelController.DefaultHandler = new PropertyChangedEventHandler(PropertyChangedEventHandlerDelegate);

                //tell controller how settings should notify view about persisted properties
                SettingsController.DefaultHandler = new PropertyChangedEventHandler(PropertyChangedEventHandlerDelegate);

                InitModelAndSettings();

                //settings used with file dialog interactions
                settingsFileDialogInfo =
                    new FileDialogInfo
                    (
                        /*parent:*/ this,
                        /*modal:*/ true,
                        /*title:*/ null,
                        /*response:*/ DialogResult.None,
                        /*newFilename:*/ SettingsController.FILE_NEW,
                        /*filename:*/ null,
                        /*extension:*/ MVCSettings.FileTypeExtension,
                        /*description:*/ MVCSettings.FileTypeDescription,
                        /*typeName:*/ MVCSettings.FileTypeName,
                        /*additionalFilters:*/ new String[] 
                        { 
                            //"MvcSettings files (*.mvcsettings)|*.mvcsettings", //default (xml) defined in SettingsBase, overridden in MVCSettings
                            //"JSON files (*.json)|*.json", //Note:need to find JSON support in Mono
                            "XML files (*.xml)|*.xml", 
                            "All files (*.*)|*.*" 
                        },
                        /*multiselect:*/ false,
                        /*initialDirectory:*/ Environment.SpecialFolder.Personal,
                        /*forceDialog:*/ false,
                        /*forceNew:*/ false,
                        /*customInitialDirectory:*/ false//PathExtensions.WithTrailingSeparator(Environment.GetFolderPath(Environment.SpecialFolder.Personal))
                    );

                //set dialog caption
                settingsFileDialogInfo.Title = this.Text;

                actionIconImages = new ListDictionary();
                //get from Resources; error conversting from Icon to Image
                // { "App", imageList1.Images[0]App },
                actionIconImages.Add("New", imageList1.Images[1] );
                actionIconImages.Add("Open", imageList1.Images[2]);
                actionIconImages.Add("Save", imageList1.Images[3]);
                actionIconImages.Add("Print", imageList1.Images[4]);
                actionIconImages.Add("Undo", imageList1.Images[5]);
                actionIconImages.Add("Redo", imageList1.Images[6]);
                actionIconImages.Add("Cut", imageList1.Images[7]);
                actionIconImages.Add("Copy", imageList1.Images[8]);
                actionIconImages.Add("Paste", imageList1.Images[9]);
                actionIconImages.Add("Delete", imageList1.Images[10]);
                actionIconImages.Add("Find", imageList1.Images[11]);
                actionIconImages.Add("Replace", imageList1.Images[12]);
                actionIconImages.Add("Refresh", imageList1.Images[13]);
                actionIconImages.Add("Preferences", imageList1.Images[14]);
                actionIconImages.Add("Properties", imageList1.Images[15]);
                actionIconImages.Add("Contents", imageList1.Images[16]);
                actionIconImages.Add("About", imageList1.Images[17]);
                actionIconImages.Add("Network", imageList1.Images[18]);
                
                //class to handle standard behaviors
                ViewModelController.New
                (
                    ViewName,
                    new MVCViewModel
                    (
                        new PropertyChangedEventHandler(this.PropertyChangedEventHandlerDelegate),
                        actionIconImages,
                        settingsFileDialogInfo,
                        this
                    )
                );

                //select a viewmodel by view name
                ViewModel = (MVCViewModel)ViewModelController.ViewModel[ViewName];

                BindFormUi();

                //Init config parameters
                if (!LoadParameters())
                {
                    throw new Exception(String.Format("Unable to load config file parameter(s)."));
                }

                //DEBUG:filename coming in is being converted/passed as DOS 8.3 format equivalent
                //Load
                if ((SettingsController.FilePath == null) || (SettingsController.Filename == SettingsController.FILE_NEW))
                {
                    //NEW
                    ViewModel.FileNew();
                }
                else
                {
                    //OPEN
                    ViewModel.FileOpen(false);
                }

#if debug
            //debug view
            menuEditProperties_Click(sender, e);
#endif

                //Display dirty state
                ModelController.Model.Refresh();
            }
            catch (Exception ex)
            {
                if (ViewModel != null)
                {
                    ViewModel.ErrorMessage = ex.Message;
                }
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }
        }

        protected void InitModelAndSettings()
        {
            //create Settings before first use by Model
            if (SettingsController.Settings == null)
            {
                SettingsController.New();
            }
            //Model properties rely on Settings, so don't call Refresh before this is run.
            if (ModelController.Model == null)
            {
                ModelController.New();
            }
        }

        protected void DisposeSettings()
        {
            MessageDialogInfo questionMessageDialogInfo = null;
            String errorMessage = null;

            //save user and application settings 
            // MvcForms.WinForm.Properties.Settings.Default.Save();//BUG:seems to be ignored

            if (SettingsController.Settings.Dirty)
            {
                //prompt before saving
                questionMessageDialogInfo = new MessageDialogInfo
                (
                    this,
                    true,
                    this.Text,
                    null,
                    MessageBoxIcon.Question,
                    MessageBoxButtons.YesNo,
                    "Save changes?",
                    DialogResult.None
                );
                if (!Dialogs.ShowMessageDialog(ref questionMessageDialogInfo, ref errorMessage))
                {
                    throw new ApplicationException(errorMessage);
                }

                // DialogResult dialogResult = MessageBox.Show("Save changes?", this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                switch (questionMessageDialogInfo.Response)
                {
                    case DialogResult.Yes:
                        {
                            //SAVE
                            ViewModel.FileSave();

                            break;
                        }
                    case DialogResult.No:
                        {
                            break;
                        }
                    default:
                        {
                            throw new InvalidEnumArgumentException();
                        }
                }
            }

            //unsubscribe from model notifications
            ModelController.Model.PropertyChanged -= new PropertyChangedEventHandler(PropertyChangedEventHandlerDelegate);//Note:will this find/remove anything?
        }

        protected void _Run()
        {
            //MessageBox.Show("running", "MVC", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
        }
        #endregion FormAppBase

        #region Utility
        /// <summary>
        /// Bind static Model controls to Model Controller
        /// </summary>
        private void BindFormUi()
        {
            try
            {
                //Form

                //Controls
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        /// Bind Model controls to Model Controller
        /// Note: databinding not used in this implementation, but leave this in place 
        ///  in case you want to do this
        /// </summary>
        private void BindModelUi()
        {
            try
            {
                BindField(txtSomeInt, ModelController.Model, "SomeInt", "Text", false, true);
                BindField(txtSomeString, ModelController.Model, "SomeString", "Text", false, true);
                BindField(chkSomeBoolean, ModelController.Model, "SomeBoolean", "Checked", false, true);

                BindField(txtSomeOtherInt, ModelController.Model, "SomeComponent.SomeOtherInt", "Text", false, true);
                BindField(txtSomeOtherString, ModelController.Model, "SomeComponent.SomeOtherString", "Text", false, true);
                BindField(chkSomeOtherBoolean, ModelController.Model, "SomeComponent.SomeOtherBoolean", "Checked", false, true);
                
                BindField(txtStillAnotherInt, ModelController.Model, "StillAnotherComponent.StillAnotherInt", "Text", false, true);
                BindField(txtStillAnotherString, ModelController.Model, "StillAnotherComponent.StillAnotherString", "Text", false, true);
                BindField(chkStillAnotherBoolean, ModelController.Model, "StillAnotherComponent.StillAnotherBoolean", "Checked", false, true);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// Note: databinding not use in Mono implementation, but leave this in place 
        ///  in case you want to do this
        private void BindField
        (
            Control fieldControl,
            MVCModel model,
            String modelPropertyName,
            String controlPropertyName,// = "Text",
            Boolean formattingEnabled,// = false,
            //DataSourceUpdateMode dataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged,
            Boolean reBind// = true
        )
        {
            try
            {
                //TODO: .RemoveSignalHandler ?
                //fieldControl.DataBindings.Clear();
                if (reBind)
                {
                    //TODO:.AddSignalHandler ?
                    //fieldControl.DataBindings.Add(controlPropertyName, model, modelPropertyName, formattingEnabled, dataSourceUpdateMode);
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// Apply Settings to viewer.
        /// </summary>
        private void  ApplySettings()
        {
            try
            {
                // _ValueChangedProgrammatically = true;

                //apply settings that have databindings
                BindModelUi();

                //apply settings that shouldn't use databindings

                //apply settings that can't use databindings
                Text = System.IO.Path.GetFileName(SettingsController.Filename) + " - " + ViewName;

                //apply settings that don't have databindings
                ViewModel.DirtyIconIsVisible = (SettingsController.Settings.Dirty);
                //update remaining non-bound controls: textboxes, checkboxes
                //Note: ModelController.Model.Refresh() will cause SO here
                ModelController.Model.Refresh("SomeInt");
                ModelController.Model.Refresh("SomeBoolean");
                ModelController.Model.Refresh("SomeString");
                ModelController.Model.Refresh("SomeOtherInt");
                ModelController.Model.Refresh("SomeOtherBoolean");
                ModelController.Model.Refresh("SomeOtherString");
                ModelController.Model.Refresh("StillAnotherInt");
                ModelController.Model.Refresh("StillAnotherBoolean");
                ModelController.Model.Refresh("StillAnotherString");

                // _ValueChangedProgrammatically = false;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        /// Set function button and menu to enable value, and cancel button to opposite.
        /// For now, do only disabling here and leave enabling based on biz logic 
        ///  to be triggered by refresh?
        /// Note: another feature that is not used, but could be
        /// </summary>
        /// <param name="functionButton"></param>
        /// <param name="functionMenu"></param>
        /// <param name="cancelButton"></param>
        /// <param name="enable"></param>
        private void SetFunctionControlsEnable
        (
            Button functionButton,
            Button functionToolbarButton,
            MenuItem functionMenu,
            Button cancelButton,
            Boolean enable
        )
        {
            try
            {
                //stand-alone button
                if (functionButton != null)
                {
                    functionButton.Enabled = enable;
                }

                //toolbar button
                if (functionToolbarButton != null)
                {
                    functionToolbarButton.Enabled = enable;
                }

                //menu item
                if (functionMenu != null)
                {
                    functionMenu.Enabled = enable;
                }

                //stand-alone cancel button
                if (cancelButton != null)
                {
                    cancelButton.Enabled = !enable;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// Invoke any delegate that has been registered 
        ///  to cancel a long-running background process.
        /// Note: another feature that is not used, but could be
        /// </summary>
        // private void InvokeActionCancel()
        // {
        //     try
        //     {
        //         //execute cancellation hook
        //         if (cancelDelegate != null)
        //         {
        //             cancelDelegate();
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
        //     }
        // }

        /// <summary>
        /// Load from app config; override with command line if present
        /// </summary>
        /// <returns></returns>
        private Boolean LoadParameters()
        {
            Boolean returnValue = false;
#if USE_CONFIG_FILEPATH
            String _settingsFilename = null;
            String _settingsSerializeAs = null;
#endif

            try
            {
                if ((ProgramBase.Filename != null) && (ProgramBase.Filename != SettingsController.FILE_NEW))
                {
                    //got filename from command line
                    //DEBUG:filename coming in is being converted/passed as DOS 8.3 format equivalent
                    // if (!RegistryAccess.ValidateFileAssociation(Application.ExecutablePath, "." + MVCSettings.FileTypeExtension))
                    // {
                    //     throw new ApplicationException(String.Format("Settings filename not associated: '{0}'.\nCheck filename on command line.", Program.Filename));
                    // }
                    //it passed; use value from command line
                }
                else
                {
#if USE_CONFIG_FILEPATH
                    //get filename from app.config
                    if (!Configuration.ReadString("SettingsFilename", out _settingsFilename))
                    {
                        throw new ApplicationException(String.Format("Unable to load SettingsFilename: {0}", "SettingsFilename"));
                    }
                    if ((_settingsFilename == null) || (_settingsFilename == SettingsController.FILE_NEW))
                    {
                        throw new ApplicationException(String.Format("Settings filename not set: '{0}'.\nCheck SettingsFilename in app config file.", _settingsFilename));
                    }
                    //use with the supplied path
                    SettingsController.Filename = _settingsFilename;
Console.WriteLine("SettingsFilename="+SettingsController.Filename);
                    //get serialization format from app.config
                    if (!Configuration.ReadString("SettingsSerializeAs", out _settingsSerializeAs))
                    {
                        throw new ApplicationException(String.Format("Unable to load SettingsSerializeAs: {0}", "SettingsSerializeAs"));
                    }
                    if ((_settingsSerializeAs == null) || (_settingsSerializeAs == ""))
                    {
                        throw new ApplicationException(String.Format("Settings serialization format not set: '{0}'.\nCheck SettingsSerializeAs in app config file.", _settingsSerializeAs));
                    }
                    //use with the filename
                    MVCSettings.SerializeAs = MVCSettings.ToSerializationFormat(_settingsSerializeAs);
Console.WriteLine("SerializeAs="+MVCSettings.SerializeAs);

                    if (System.IO.Path.GetDirectoryName(_settingsFilename) == String.Empty)
                    {
                        //supply default path if missing
                        SettingsController.Pathname = Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator();
                    }
#endif
                }

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                //throw;
            }
            return returnValue;
        }

        private void BindSizeAndLocation()
        {

            //Note:Size must be done after InitializeComponent(); do Location this way as well.--SJS
            // this.DataBindings.Add(new System.Windows.Forms.Binding("Location", global::MVCForms.Properties.Settings.Default, "Location", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            // this.DataBindings.Add(new System.Windows.Forms.Binding("ClientSize", global::MVCForms.Properties.Settings.Default, "Size", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            // Console.WriteLine("Width="+global::MvcForms.WinForm.Properties.Settings.Default.Size.Width);
            // Console.WriteLine("Height="+global::MvcForms.WinForm.Properties.Settings.Default.Size.Height);
            // this.Size = //BUG:seems to be ignored
            //     new Size
            //     (
            //         global::MvcForms.WinForm.Properties.Settings.Default.Size.Width,
            //         global::MvcForms.WinForm.Properties.Settings.Default.Size.Height
            //     );
            // this.Location = 
            //     new Point
            //     (
            //         global::MvcForms.WinForm.Properties.Settings.Default.Location.X,
            //         global::MvcForms.WinForm.Properties.Settings.Default.Location.Y
            //     );
        }
        #endregion Utility
        #endregion Methods

        #region Actions

        #endregion Actions

        #region Utility
        private void StartProgressBar()
        {
            progressBar.Value = 33;
//            progressBar.Style = ProgressBarStyle.Marquee;//true;
            progressBar.Visible = true;
            //DoEvents;
        }

        private void StopProgressBar()
        {
            //DoEvents;
            progressBar.Visible = false;
        }

        private void StartActionIcon(Image resourceItem/*String resourceItemId*/)
        {
            actionIcon.Image = (resourceItem == null ? null : resourceItem);//imageList1.Images[0]New; //"New", etc.
            actionIcon.Visible = true;
        }

        private void StopActionIcon()
        {
            actionIcon.Visible = false;
            actionIcon.Image = imageList1.Images[0]; 
        }

        #endregion Utility

	}
}
