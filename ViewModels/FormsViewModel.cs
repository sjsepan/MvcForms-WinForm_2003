﻿using System;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using System.Threading;
using Ssepan.Utility;
using Ssepan.Application;
using Ssepan.Ui.WinForm;
using MvcLibrary;

namespace MvcForms.WinForm
{
    /// <summary>
    /// Note: this class can be subclassed without type parameters in the client.
    /// </summary>
    /// <typeparam name="Icon"></typeparam>
    /// <typeparam name="MVCSettings"></typeparam>
    /// <typeparam name="MVCModel"></typeparam>
    /// <typeparam name="Form"></typeparam>
    public class FormsViewModel	:
		ViewModelBase
	{
        #region Declarations
//        public delegate Boolean DoWork_WorkDelegate(BackgroundWorker worker, DoWorkEventArgs e, ref String errorMessage);

        protected static FileDialogInfo _settingsFileDialogInfo = null;
        protected ListDictionary _actionIconImages = null;

        #endregion Declarations

        #region Constructors
        public FormsViewModel()        {
            if (SettingsController.Settings == null)
            {
                SettingsController.New();
            }

        }

        public FormsViewModel
        (
            PropertyChangedEventHandler propertyChangedEventHandlerDelegate,
            ListDictionary actionIconImages,
            FileDialogInfo settingsFileDialogInfo
        ) :
            this()
        {
            try
            {
                //(and the delegate it contains
                if (propertyChangedEventHandlerDelegate != null)
                {
                    this.PropertyChanged += new PropertyChangedEventHandler(propertyChangedEventHandlerDelegate);
                }

                _actionIconImages = actionIconImages;

                _settingsFileDialogInfo = settingsFileDialogInfo;

                ActionIconImage = (Bitmap)_actionIconImages["Save"];
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }
        }

        public FormsViewModel
        (
            PropertyChangedEventHandler propertyChangedEventHandlerDelegate,
            ListDictionary actionIconImages,
            FileDialogInfo settingsFileDialogInfo,
            Form view //= default(Form) //(In VB 2010, )VB caller cannot differentiate between members which differ only by an optional param--SJS
        ) :
            this(propertyChangedEventHandlerDelegate, actionIconImages, settingsFileDialogInfo)
        {
            try
            {
                View = view;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }
        }
        #endregion Constructors

        #region Properties
        private Form _View = null;
        public Form View 
        {
	        get { return _View; } 
	        set { _View = value; } 
        }
        #endregion Properties

        #region Methods
        #region Menus
        #region menu feature boilerplate
        //may be used as-is, or overridden to handle differently

        public virtual void FileNew()
        {
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;
            MessageDialogInfo messageDialogInfo = null;
            String errorMessage = null;
            try
            {
                StartProgressBar
                (
                    "New" + ACTION_IN_PROGRESS, 
                    null, 
                    (Bitmap)_actionIconImages["New"], 
                    true, 
                    33
                );

                if (SettingsController.Settings.Dirty)
                {
                    //prompt before saving
                    messageDialogInfo = 
                        new MessageDialogInfo
                        (
                            /*parent:*/ View,//saved Window reference
                            /*modal:*/ true,
                            /*title:*/ "New",
                            /*dialogFlags:*/ null,
                            /*messageType:*/ MessageBoxIcon.Question,
                            /*buttonsType:*/ MessageBoxButtons.YesNo,
                            /*message:*/ "Save changes?",
                            /*response:*/ DialogResult.None
                        );
                    if (!Dialogs.ShowMessageDialog
                    (
                        ref messageDialogInfo,
                        ref errorMessage
                    ))
                    {
                        throw new ApplicationException(errorMessage);
                    }; 

                    if (messageDialogInfo.Response == DialogResult.Yes)
                    {
                        //SAVE
                        FileSaveAs();
                    }
                    //else treat as cancel
                }

                //NEW
                if (!SettingsController.New())
                {
                    throw new ApplicationException(String.Format("Unable to get New settings.\r\nPath: {0}", SettingsController.FilePath));
                }

                ModelController.Model.Refresh();

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                
                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
        }

		/// <summary>
		/// Open object at SettingsController.FilePath.
		/// </summary>
		/// <param name="forceDialog">Default parameter = true</param>
		public virtual void FileOpen()
		{
			FileOpen(true);
		}

        /// <summary>
        /// Open object at SettingsController.FilePath.
        /// </summary>
        /// <param name="forceDialog">If false, just use SettingsController.FilePath.</param>
        public virtual void FileOpen(Boolean forceDialog)
        {
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;
            MessageDialogInfo messageDialogInfo = null;
            String errorMessage = null;

            try
            {
                StartProgressBar
                (
                    "Open" + ACTION_IN_PROGRESS,
                    null,
                    (Bitmap)_actionIconImages["Open"], 
                    true,
                    33
                );

                if (SettingsController.Settings.Dirty)
                {
                    //prompt before saving
                    messageDialogInfo = 
                        new MessageDialogInfo
                        (
                            /*parent:*/ View,//saved Window reference
                            /*modal:*/ true,
                            /*title:*/ "Save",
                            /*dialogFlags:*/ null,
                            /*messageType:*/ MessageBoxIcon.Question,
                            /*buttonsType:*/ MessageBoxButtons.YesNo,
                            /*message:*/ "Save changes?",
                            /*response:*/ DialogResult.None
                        );

                    if (!Dialogs.ShowMessageDialog(ref messageDialogInfo,ref errorMessage))
                    {
                        throw new ApplicationException(errorMessage);
                    }; 

                    if (messageDialogInfo.Response == DialogResult.Yes)
                    {
                        //SAVE
                        FileSave();
                    }
                    //else treat as cancel
                }

                if (forceDialog)
                {
                    _settingsFileDialogInfo.Title = "Open";
                    _settingsFileDialogInfo.Filename = SettingsController.FilePath;
                    
                    if (!Dialogs.GetPathForLoad(ref _settingsFileDialogInfo, ref errorMessage))
                    {
                        throw new ApplicationException(String.Format("GetPathForLoad: {0}", errorMessage));
                    }

                    if (_settingsFileDialogInfo.Response != DialogResult.None)
                    {
                        if (_settingsFileDialogInfo.Response == DialogResult.OK)
                        {
                            SettingsController.FilePath = _settingsFileDialogInfo.Filename;
                            UpdateStatusBarMessages(StatusMessage + _settingsFileDialogInfo.Filename + ACTION_IN_PROGRESS, null);
                        }
                        else
                        {
                            //treat as cancel
                            StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                            return; //if open was cancelled
                        }
                    }
                    else
                    {
                        //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if open was cancelled
                    }
                }

                //OPEN
                if (!SettingsController.Open())
                {
                    throw new ApplicationException(String.Format("Unable to Open settings.\r\nPath: {0}", SettingsController.FilePath));
                }

                ModelController.Model.Refresh();

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
        }

		/// <summary>
		/// Save object at SettingsController.FilePath.
		/// </summary>
		/// <param name="isSaveAs"><Default parameter = trfalseue</param>
		public virtual void FileSave()
		{
			FileSave(false);
		}

		/// <summary>
		/// Save object at SettingsController.FilePath.
		/// </summary>
		/// <param name="isSaveAs"></param>
		public virtual void FileSave(Boolean isSaveAs)
        {
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;
            String errorMessage = null;

            try
            {
                StartProgressBar
                (
                    "Save" + ACTION_IN_PROGRESS,
                    null,
                    (Bitmap)_actionIconImages["Save"], 
                    true,
                    33
                );

                _settingsFileDialogInfo.Title = (isSaveAs ? "Save As..." : "Save...");
                _settingsFileDialogInfo.ForceDialog = isSaveAs;
                _settingsFileDialogInfo.Modal = true;
                _settingsFileDialogInfo.Filename = SettingsController.FilePath;
                
                if (!Dialogs.GetPathForSave(ref _settingsFileDialogInfo, ref errorMessage))
                {
                    throw new ApplicationException(String.Format("GetPathForSave: {0}", errorMessage));
                }

                if (_settingsFileDialogInfo.Response != DialogResult.None)
                {
                    if (_settingsFileDialogInfo.Response == DialogResult.OK)
                    {
                        SettingsController.FilePath = _settingsFileDialogInfo.Filename;
                        UpdateStatusBarMessages(StatusMessage + _settingsFileDialogInfo.Filename + ACTION_IN_PROGRESS, null);
                    }
                    else
                    {
                        //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if dialog was cancelled
                    }
                }
                else
                {
                    //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if dialog was cancelled
                }

                //SAVE
                if (!SettingsController.Save())
                {
                    throw new ApplicationException(String.Format("Unable to Save settings.\r\nPath: {0}", SettingsController.FilePath));
                }

                ModelController.Model.Refresh();

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
        }

        public virtual void FileSaveAs()
        {
            FileSave(true);
        }

        public virtual void FilePrint()
        {
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;
            PrinterDialogInfo printerDialogInfo = null;
            String errorMessage = null;

            try
            {
                StartProgressBar
                (
                    "Print" + ACTION_IN_PROGRESS,
                    null,
                    (Bitmap)_actionIconImages["Print"], 
                    true,
                    33
                );

                //select printer
                printerDialogInfo = new PrinterDialogInfo
                (
                    /*parent:*/ View,
                    /*modal:*/ true,
                    /*title:*/ "Printer",
                    /*response:*/ DialogResult.None
                );

                if (!Dialogs.GetPrinter(ref printerDialogInfo, ref errorMessage))
                {
                    throw new ApplicationException(String.Format("GetPrinter: {0}", errorMessage));
                }

                if (printerDialogInfo.Response != DialogResult.None)
                {
                    if (printerDialogInfo.Response == DialogResult.OK)
                    {
                        UpdateStatusBarMessages(StatusMessage + printerDialogInfo.Printer + ACTION_IN_PROGRESS, null);
                    }
                    else
                    {
                        //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if open was cancelled
                    }
                }
                else
                {
                    //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if open was cancelled
                }

                //PRINT
                if (Print())
                {
                    // StopProgressBar("Printed.");
                }
                else
                {
                    // StopProgressBar("Print cancelled.");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
        }

        public virtual void FilePrintPreview()
        {
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "Print Preview" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["PrintPreview"], 
                    true,
                    33
                );

                //TODO:select printer?
                
                if (PrintPreview())
                {
                    StopProgressBar(StatusMessage + ACTION_DONE);
                }
                else
                {
                    StopProgressBar(StatusMessage + ACTION_CANCELLED);
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
        }

        public virtual void FileExit
        (
            ref Boolean resultCancelQuitting
        )
        {
            String errorMessage = null;
            MessageDialogInfo messageDialogInfo = null;
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "Quit" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["Quit"], 
                    true,
                    33
                );

                //prompt before quitting
                messageDialogInfo = 
                    new MessageDialogInfo
                    (
                        /*parent:*/ View,//saved Window reference
                        /*modal:*/ true,
                        /*title:*/ "Quit?",
                        /*dialogFlags:*/ null,
                        /*messageType:*/ MessageBoxIcon.Question,
                        /*buttonsType:*/ MessageBoxButtons.YesNo,
                        /*message:*/ "Are you sure you want to quit?",
                        /*response:*/ DialogResult.None
                    );

                if (!Dialogs.ShowMessageDialog(ref messageDialogInfo, ref errorMessage))
                {
                    throw new ApplicationException(errorMessage);
                }; 
                
                if (messageDialogInfo.Response == DialogResult.No)
                {
                    resultCancelQuitting = true;
                    StopProgressBar(StatusMessage + ACTION_CANCELLED);
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = String.Format("{0}", ex.Message);

                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }
        }

        public virtual void EditUndo()
        {
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "Undo" + ACTION_IN_PROGRESS,
                    null,
                    (Bitmap)_actionIconImages["Undo"],
                    true,
                    33
                );

                if (!Undo())
                {
                    throw new ApplicationException("'Undo' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
        }

        public virtual void EditRedo()
        {
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "Redo" + ACTION_IN_PROGRESS,
                    null,
                    (Bitmap)_actionIconImages["Redo"],
                    true,
                    33
                );

                if (!Redo())
                {
                    throw new ApplicationException("'Redo' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
        }

        public virtual void EditSelectAll()
        {
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "Select All" + ACTION_IN_PROGRESS,
                    null,
                    null,//(Bitmap)_actionIconImages["SelectAll"],
                    true,
                    33
                );

                if (!SelectAll())
                {
                    throw new ApplicationException("'Select All' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
        }

        public virtual void EditCut()
        {
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "Cut" + ACTION_IN_PROGRESS,
                    null,
                    (Bitmap)_actionIconImages["Cut"],
                    true,
                    33
                );

                if (!Cut())
                {
                    throw new ApplicationException("'Cut' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
        }

        public virtual void EditCopy()
        {
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "Copy" + ACTION_IN_PROGRESS,
                    null,
                    (Bitmap)_actionIconImages["Copy"],
                    true,
                    33
                );

                if (!Copy())
                {
                    throw new ApplicationException("'Copy' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
        }

        public virtual void EditPaste()
        {
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "Paste" + ACTION_IN_PROGRESS,
                    null,
                    (Bitmap)_actionIconImages["Paste"],
                    true,
                    33
                );

                if (!Paste())
                {
                    throw new ApplicationException("'Paste' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
        }

        public virtual void EditPasteSpecial()
        {
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "Past Special" + ACTION_IN_PROGRESS,
                    null,
                    null,//(Bitmap)_actionIconImages["PasteSpecial"],
                    true,
                    33
                );

                if (!PasteSpecial())
                {
                    throw new ApplicationException("'Paste Special' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
        }

        public virtual void EditDelete()
        {
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "Delete" + ACTION_IN_PROGRESS,
                    null,
                    (Bitmap)_actionIconImages["Delete"],
                    true,
                    33
                );

                if (!Delete())
                {
                    throw new ApplicationException("'Delete' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
        }

        public virtual void EditFind()
        {
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "Find" + ACTION_IN_PROGRESS,
                    null,
                    (Bitmap)_actionIconImages["Find"],
                    true,
                    33
                );

                if (!Find())
                {
                    throw new ApplicationException("'Find' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
        }

        public virtual void EditReplace()
        {
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "Replace" + ACTION_IN_PROGRESS,
                    null,
                    (Bitmap)_actionIconImages["Replace"],
                    true,
                    33
                );

                if (!Replace())
                {
                    throw new ApplicationException("'Replace' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
        }

        public virtual void EditRefresh()
        {
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "Refresh" + ACTION_IN_PROGRESS,
                    null,
                    (Bitmap)_actionIconImages["Refresh"],
                    true,
                    33
                );

                if (!Refresh())
                {
                    throw new ApplicationException("'Refresh' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
        }

        public virtual void EditPreferences()
        {
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "Preferences" + ACTION_IN_PROGRESS,
                    null,
                    (Bitmap)_actionIconImages["Preferences"],
                    true,
                    33
                );

                if (!Preferences())
                {
                    throw new ApplicationException("'Preferences' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
        }

        public virtual void EditProperties()
        {
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "Properties" + ACTION_IN_PROGRESS,
                    null,
                    (Bitmap)_actionIconImages["Properties"],
                    true,
                    33
                );

                if (!Properties())
                {
                    throw new ApplicationException("'Properties' error");
                }

                //TODO:when implemented, a Properties dialog can also be cancelled, so handle cancel too
                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
        }

        public virtual void WindowNewWindow()
        {
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "New Window" + ACTION_IN_PROGRESS,
                    null,
                    null,//(Bitmap)_actionIconImages["NewWindow"],
                    true,
                    33
                );

                if (!NewWindow())
                {
                    throw new ApplicationException("'New Window' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
        }

        public virtual void WindowTile()
        {
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "Tile" + ACTION_IN_PROGRESS,
                    null,
                    null,//(Bitmap)_actionIconImages["Tile"],
                    true,
                    33
                );

                if (!Tile())
                {
                    throw new ApplicationException("'Tile' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
        }

        public virtual void WindowCascade()
        {
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "Cascade" + ACTION_IN_PROGRESS,
                    null,
                    null,//(Bitmap)_actionIconImages["Cascade"],
                    true,
                    33
                );

                if (!Cascade())
                {
                    throw new ApplicationException("'Cascade' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
        }

        public virtual void WindowArrangeAll()
        {
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "Arrange All" + ACTION_IN_PROGRESS,
                    null,
                    null,//(Bitmap)_actionIconImages["ArrangeAll"],
                    true,
                    33
                );

                if (!ArrangeAll())
                {
                    throw new ApplicationException("'Arrange All' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
        }

        public virtual void WindowHide()
        {
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "Hide" + ACTION_IN_PROGRESS,
                    null,
                    null,//(Bitmap)_actionIconImages["Hide"],
                    true,
                    33
                );

                if (!Hide())
                {
                    throw new ApplicationException("'Hide' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
        }

        public virtual void WindowShow()
        {
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "Show" + ACTION_IN_PROGRESS,
                    null,
                    null,//(Bitmap)_actionIconImages["Show"],
                    true,
                    33
                );

                if (!Show())
                {
                    throw new ApplicationException("'Show' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
        }

        public virtual void HelpContents()
        {
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "Contents" + ACTION_IN_PROGRESS,
                    null,
                    (Bitmap)_actionIconImages["Contents"],
                    true,
                    33
                );
                //System.Windows.Forms.Help
                if (!Contents())
                {
                    throw new ApplicationException("'Help Contents' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
        }

        public virtual void HelpIndex()
        {
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "Index" + ACTION_IN_PROGRESS,
                    null,
                    null,//(Bitmap)_actionIconImages["Index"],
                    true,
                    33
                );
                //System.Windows.Forms.Help
                if (!Index())
                {
                    throw new ApplicationException("'Help Index' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
        }

        public virtual void HelpOnlineHelp()
        {
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "Online Help" + ACTION_IN_PROGRESS,
                    null,
                    null,//(Bitmap)_actionIconImages["HelpOnlineHelp"],
                    true,
                    33
                );

                //System.Windows.Forms.Help
                if (!OnlineHelp())
                {
                    throw new ApplicationException("'Online Help' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
        }

        public virtual void HelpLicenceInformation()
        {
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "Licence Information" + ACTION_IN_PROGRESS,
                    null,
                    null,//(Bitmap)_actionIconImages["LicenceInformation"],
                    true,
                    33
                );

                if (!LicenceInformation())
                {
                    throw new ApplicationException("'Licence Information' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
        }

        public virtual void HelpCheckForUpdates()
        {
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "Check For Updates" + ACTION_IN_PROGRESS,
                    null,
                    null,//(Bitmap)_actionIconImages["CheckForUpdates"],
                    true,
                    33
                );

                if (!CheckForUpdates())
                {
                    throw new ApplicationException("'Check For Updates' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
        }

        // public virtual void HelpAbout()
        //     where TAssemblyInfo :
        //     //class,
        //     AssemblyInfoBase,
        //     new()
        // {
        //     StatusMessage = String.Empty;
        //     ErrorMessage = String.Empty;
        //     TAssemblyInfo assemblyInfo = null;
        //     AboutDialogInfo aboutDialogInfo = null;
        //     DialogResult response = DialogResult.None;
        //     String errorMessage = null;

        //     try
        //     {

        //         StartProgressBar
        //         (
        //             "About" + ACTION_IN_PROGRESS, 
        //             null, 
        //             (Bitmap)_actionIconImages["About"], 
        //             true, 
        //             33
        //         );

        //         //assemblyInfo NOT passed in;specifically, NOT called by override of HelpAbout()
        //         assemblyInfo = new TAssemblyInfo();
                
        //         // use GUI mode About feature
        //         aboutDialogInfo = 
        //             new AboutDialogInfo
        //             (
        //                 parent : this.View,
        //                 response : response,
        //                 modal : true,
        //                 title : "About " + assemblyInfo.Title,
        //                 programName : assemblyInfo.Product,
        //                 version : assemblyInfo.Version,
        //                 copyright : assemblyInfo.Copyright,
        //                 comments : assemblyInfo.Description,
        //                 website : assemblyInfo.Website,
        //                 //Note:cannot access logo from loaded resources
        //                 logo : null,
        //                 websiteLabel : assemblyInfo.WebsiteLabel,
        //                 designers : assemblyInfo.Designers,
        //                 developers : assemblyInfo.Developers,
        //                 documenters : assemblyInfo.Documenters,
        //                 license : assemblyInfo.License
        //             );

        //         Debug.Assert(assemblyInfo != null);

        //         if (!Dialogs.ShowAboutDialog(ref aboutDialogInfo, ref errorMessage))
        //         {
        //             throw new ApplicationException(errorMessage);
        //         }
                
        // //         StopProgressBar(StatusMessage + ACTION_DONE);
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

        //         StopProgressBar(null, String.Format("{0}", ex.Message));
        //     }
        // }
        #endregion menu feature boilerplate

        #region menu feature implementation
        //likely to need overridden to handle feature specifics

        /// <summary>
        /// placeholder and example, but this must be overridden to actually do anything
        /// with the print dialog, such as sending a document to the printer
        /// </summary>
        /// <returns>Boolean</returns>
        protected virtual Boolean Print()
        {
            Boolean returnValue = false;

            try
            {
                    returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                
            }
            // finally
            // {
            // }

            return returnValue;
        }

        /// <summary>
        /// placeholder and example, but this must be overridden to actually do anything
        /// </summary>
        /// <returns>Boolean</returns>
        protected virtual Boolean PrintPreview()
        {
            Boolean returnValue = false;
            // String errorMessage = null;
            // PrinterDialogInfo printerDialogInfo = null;

            try
            {
                // if (Dialogs.GetPrinter(ref printerDialogInfo, ref errorMessage))
                // {
                //     Log.Write(printerDialogInfo.Printer.Name,  Log.EventLogEntryType_Error);
                    
                     returnValue = true;
                // }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                
            }
            finally
            {
            }

            return returnValue;
        }

        protected virtual Boolean Undo()
        {
            Boolean returnValue = false;

            try
            {
                DoSomethingElse(); //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                
            }

            return returnValue;
        }

        protected virtual Boolean Redo()
        {
            Boolean returnValue = false;

            try
            {
                DoSomethingElse(); //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                
            }

            return returnValue;
        }

        protected virtual Boolean SelectAll()
        {
            Boolean returnValue = false;

            try
            {
                DoSomethingElse(); //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                
            }

            return returnValue;
        }

        protected virtual Boolean Cut()
        {
            Boolean returnValue = false;

            try
            {
                DoSomethingElse(); //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                
            }

            return returnValue;
        }

        protected virtual Boolean Copy()
        {
            Boolean returnValue = false;

            try
            {
                DoSomethingElse(); //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                
            }

            return returnValue;
        }

        protected virtual Boolean Paste()
        {
            Boolean returnValue = false;

            try
            {
                DoSomethingElse(); //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                
            }

            return returnValue;
        }

        protected virtual Boolean PasteSpecial()
        {
            Boolean returnValue = false;

            try
            {
                DoSomethingElse(); //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                
            }

            return returnValue;
        }

        protected virtual Boolean Delete()
        {
            Boolean returnValue = false;

            try
            {
                DoSomethingElse(); //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                
            }

            return returnValue;
        }

        protected virtual Boolean Find()
        {
            Boolean returnValue = false;

            try
            {
                DoSomethingElse(); //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                
            }

            return returnValue;
        }

        protected virtual Boolean Replace()
        {
            Boolean returnValue = false;

            try
            {
                DoSomethingElse(); //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                
            }

            return returnValue;
        }

        protected virtual Boolean Refresh()
        {
            Boolean returnValue = false;

            try
            {
                DoSomethingElse(); //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                
            }

            return returnValue;
        }

        protected virtual Boolean Preferences()
        {
            Boolean returnValue = false;

            try
            {
                DoSomethingElse(); //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                
            }

            return returnValue;
        }

        protected virtual Boolean Properties()
        {
            Boolean returnValue = false;

            try
            {
                DoSomethingElse(); //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                
            }

            return returnValue;
        }

        protected virtual Boolean NewWindow()
        {
            Boolean returnValue = false;

            try
            {
                DoSomethingElse(); //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                
            }

            return returnValue;
        }

        protected virtual Boolean Tile()
        {
            Boolean returnValue = false;

            try
            {
                DoSomethingElse(); //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                
            }

            return returnValue;
        }

        protected virtual Boolean Cascade()
        {
            Boolean returnValue = false;

            try
            {
                DoSomethingElse(); //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                
            }

            return returnValue;
        }

        protected virtual Boolean ArrangeAll()
        {
            Boolean returnValue = false;

            try
            {
                DoSomethingElse(); //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                
            }

            return returnValue;
        }

        protected virtual Boolean Hide()
        {
            Boolean returnValue = false;

            try
            {
                DoSomethingElse(); //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                
            }

            return returnValue;
        }

        protected virtual Boolean Show()
        {
            Boolean returnValue = false;

            try
            {
                DoSomethingElse(); //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                
            }

            return returnValue;
        }

        protected virtual Boolean Contents()
        {
            Boolean returnValue = false;

            try
            {
                DoSomethingElse(); //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                
            }

            return returnValue;
        }

        protected virtual Boolean Index()
        {
            Boolean returnValue = false;

            try
            {
                DoSomethingElse(); //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                
            }

            return returnValue;
        }

        protected virtual Boolean OnlineHelp()
        {
            Boolean returnValue = false;

            try
            {
                DoSomethingElse(); //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                
            }

            return returnValue;
        }

        protected virtual Boolean LicenceInformation()
        {
            Boolean returnValue = false;

            try
            {
                DoSomethingElse(); //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                
            }

            return returnValue;
        }

        protected virtual Boolean CheckForUpdates()
        {
            Boolean returnValue = false;

            try
            {
                DoSomethingElse(); //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                
            }

            return returnValue;
        }
        #endregion menu feature implementation

        #endregion Menus

        #region Controls

//		/// <summary>
//		/// Handle DoWork event.
//		/// </summary>
//		/// <typeparam name="TReturn"></typeparam>
//		/// <param name="worker"></param>
//		/// <param name="e"></param>
//		/// <param name="workDelegate"></param>
//		/// <param name="resultNullDescription">Default parameter = "No result was returned."</param>
//		public virtual void BackgroundWorker_DoWork
//		(
//			BackgroundWorker worker, 
//			DoWorkEventArgs e, 
//			DoWork_WorkDelegate workDelegate
//		)
//		{
//			BackgroundWorker_DoWork(worker, e, workDelegate, "No result was returned.");
//		}
//
//        /// <summary>
//        /// Handle DoWork event.
//        /// </summary>
//        /// <typeparam name="TReturn"></typeparam>
//        /// <param name="worker"></param>
//        /// <param name="e"></param>
//        /// <param name="workDelegate"></param>
//        /// <param name="resultNullDescription"></param>
//        public virtual void BackgroundWorker_DoWork
//        (
//            BackgroundWorker worker, 
//            DoWorkEventArgs e, 
//            DoWork_WorkDelegate workDelegate,
//            String resultNullDescription
//        )
//        {
//            String errorMessage = null;
//
//            try
//            {
//                //run process
//                if (workDelegate != null)
//                {
//                    e.Result =
//                        workDelegate
//                        (
//                            worker,
//                            e,
//                            ref errorMessage
//                        );
//                }
//
//                //look for specific problem
//                if (!String.IsNullOrEmpty(errorMessage))
//                {
//                    throw new Exception(errorMessage);
//                }
//
//                //warn about unexpected result
//                if (e.Result == null)
//                {
//                    throw new Exception(resultNullDescription);
//                }
//            }
//            catch (Exception ex)
//            {
//                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
//
//                //re-throw and let RunWorkerCompleted event handle and report error.
//                throw;
//            }
//        }
//
//        /// <summary>
//        /// Handle ProgressChanged event.
//        /// </summary>
//        /// <param name="description"></param>
//        /// <param name="userState">Object, specifically a String.</param>
//        /// <param name="progressPercentage"></param>
//        public virtual void BackgroundWorker_ProgressChanged
//        (
//            String description, 
//            Object userState, 
//            Int32 progressPercentage
//        )
//        {
//            String message = String.Empty;
//
//            try
//            {
//                if (userState != null)
//                {
//                    message = userState.ToString();
//                }
//                UpdateProgressBar(String.Format("{0} ({1})...{2}%", description, message, progressPercentage.ToString()), progressPercentage);
//                //System.Windows.Forms.Application.DoEvents();
//            }
//            catch (Exception ex)
//            {
//                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
//            }
//        }
//
//        /// <summary>
//        /// Handle RunWorkerCompleted event.
//        /// </summary>
//        /// <param name="description"></param>
//        /// <param name="worker"></param>
//        /// <param name="e">RunWorkerCompletedEventArgs</param>
//        /// <param name="errorDelegate">Replaces default behavior of displaying the exception message.</param>
//        /// <param name="cancelledDelegate">Replaces default behavior of displaying a cancellation message. Handles the display message only; differs from cancelDelegate, which handles view-level behavior not specific to this worker.</param>
//        /// <param name="completedDelegate">Extends default behavior of refreshing the display; execute prior to Refresh().</param>
//        public virtual void BackgroundWorker_RunWorkerCompleted
//        (
//            String description,
//            BackgroundWorker worker,
//            RunWorkerCompletedEventArgs e,
//            ConsoleApplication.DVoid errorDelegate,
//            ConsoleApplication.DVoid cancelledDelegate,
//            ConsoleApplication.DVoid completedDelegate
//        )
//        {
//            try
//            {
//                Exception error = e.Error;
//                Boolean isCancelled = e.Cancelled;
//                Object result = e.Result;
//
//                // First, handle the case where an exception was thrown.
//                if (error != null)
//                {
//                    if (errorDelegate != null)
//                    {
//                        errorDelegate(error);
//                    }
//                    else
//                    {
//                        // Show the error message
//                        StopProgressBar(null, error.Message);
//                    }
//                }
//                else if (isCancelled)
//                {
//                    if (cancelledDelegate != null)
//                    {
//                        cancelledDelegate();
//                    }
//                    else
//                    {
//                        // Handle the case where the user cancelled the operation.
//                        StopProgressBar(null, "Cancelled.");
//
//                        //if (View.cancelDelegate != null)
//                        //{
//                        //    View.cancelDelegate();
//                        //}
//                    }
//                }
//                else
//                {
//                    // Operation completed successfully, so display the result.
//                    if (completedDelegate != null)
//                    {
//                        completedDelegate();
//                    }
//
//                    //background worker calls New/Save without UI refresh; refresh UI explicitly here.
//                    ModelController.Model.Refresh();
//                }
//
//                // Do post completion operations, like enabling the controls etc.      
//                View.BringToFront();
//
//                // Inform the user we're done
//                StopProgressBar(description, null);
//            }
//            catch (Exception ex)
//            {
//                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
//
//                StopProgressBar(null, String.Format("{0}", ex.Message));
//            }
//            finally
//            {
//                ////clear cancellation hook
//                //View.cancelDelegate = null;
//            }
//        }

        // /// <summary>
        // /// 
        // /// </summary>
        // public void BindingSource_PositionChanged<TItem>(BindingSource bindingSource, EventArgs e, Action<TItem> itemDelegate)
        // {
        //     try
        //     {
        //         TItem current = (TItem)bindingSource.Current;

        //         if (current != null)
        //         {
        //             if (itemDelegate != null)
        //             {
        //                 itemDelegate(current);
        //             }
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
        //     }
        // }

        // /// <summary>
        // /// Handle DataError event; specifically, catch ComboBox cell data errors.
        // /// This allows validation message to be displayed 
        // ///  and still allows the user to fix and save the settings.
        // /// </summary>
        // /// <param name="grid"></param>
        // /// <param name="e"></param>
        // /// <param name="cancel"></param>
        // /// <param name="throwException"></param>
        // /// <param name="logException"></param>
        // public void Grid_DataError
        // (
        //     DataGridView grid, 
        //     DataGridViewDataErrorEventArgs e,
        //     Boolean? cancel,
        //     Boolean? throwException,
        //     Boolean logException
        // )
        // {
        //     try
        //     {
        //         if (cancel.HasValue)
        //         {
        //             //cancel to prevent default DataError dialogs
        //             e.Cancel = true;
        //         }

        //         if (throwException.HasValue)
        //         {
        //             //there are cases where you do not want to throw exception; because it will drop application immediately with only a log
        //             e.ThrowException = throwException.Value;
        //         }

        //         if (logException)
        //         {
        //             Log.Write(e.Exception, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
        //     }
        // }
        #endregion Controls

        #region Utility

        /// <summary>
        /// Manage buttons' state while processes are running.
        /// Usage:
        ///     View.permitEnabledStateXxx = ButtonEnabled(enabledFlag, View.permitEnabledStateXxx, View.cmdXxx, View.menuFileXxx, View.buttonFileXxx);
        /// </summary>
        /// <param name="enabledFlag"></param>
        /// <param name="permitEnabledState"></param>
        /// <param name="button"></param>
        /// <param name="menuItem"></param>
        /// <param name="buttonItem"></param>
        /// <returns>updated remembered state</returns>
        public Boolean ButtonEnabled
        (
            Boolean enabledFlag,
            Boolean permitEnabledState,
            Button button,
            MenuItem menuItem,
            Button buttonItem
        )
        {
            Boolean returnValue = permitEnabledState; // false;
            try
            {
                if (enabledFlag)
                {
                    //ENABLING
                    //recall state
                    if (button != null)
                    {
                        button.Enabled = permitEnabledState;
                    }
                    if (menuItem != null)
                    {
                        menuItem.Enabled = permitEnabledState;
                    }
                    if (buttonItem != null)
                    {
                        buttonItem.Enabled = permitEnabledState;
                    }
                }
                else
                {
                    //DISABLING
                    //remember state
                    if (button != null)
                    {
                        returnValue = button.Enabled;
                    }
                    else if (menuItem != null)
                    {
                        returnValue = menuItem.Enabled;
                    }
                    else if (buttonItem != null)
                    {
                        returnValue = buttonItem.Enabled;
                    }

                    //disable
                    if (button != null)
                    {
                        button.Enabled = enabledFlag;
                    }
                    if (menuItem != null)
                    {
                        menuItem.Enabled = enabledFlag;
                    }
                    if (buttonItem != null)
                    {
                        buttonItem.Enabled = enabledFlag;
                    }
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                throw;
            }
        }

		private void DoSomethingElse()
		{
			for (int i = 0; i < 3; i++)
			{
				Application.DoEvents();
				Thread.Sleep(1000);
			}
		}
		#endregion Utility
        #endregion Methods

    }
}
