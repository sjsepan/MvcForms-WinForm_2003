﻿using System;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using Ssepan.Utility;
using Ssepan.Application;
using Ssepan.Ui.WinForm;
using MvcLibrary;

namespace MvcForms.WinForm
{
    /// <summary>
    /// Note: this class can subclass the base without type parameters.
    /// </summary>
    public class MVCViewModel :
        FormsViewModel
    {
        #region Declarations
        #endregion Declarations

        #region Constructors
        public MVCViewModel() { }//Note: not called, but need to be present to compile--SJS

        public MVCViewModel
        (
            PropertyChangedEventHandler propertyChangedEventHandlerDelegate,
            ListDictionary actionIconImages,
            FileDialogInfo settingsFileDialogInfo
        ) :
            base(propertyChangedEventHandlerDelegate, actionIconImages, settingsFileDialogInfo)
        {
            try
            {
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }
        }

        public MVCViewModel
        (
            PropertyChangedEventHandler propertyChangedEventHandlerDelegate,
            ListDictionary actionIconImages,
            FileDialogInfo settingsFileDialogInfo,
            MVCView view
        ) :
            base(propertyChangedEventHandlerDelegate, actionIconImages, settingsFileDialogInfo, view)
        {
            try
            {
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }
        }
        #endregion Constructors

        #region Properties
        #endregion Properties

        #region Methods
        /// <summary>
        /// model specific, not generic
        /// </summary>
        internal void DoSomething()
        {
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "Doing something...",
                    null,
                    null, //_actionIconImages["Xxx"],
                    true,
                    33
                );

                ModelController.Model.SomeBoolean = !ModelController.Model.SomeBoolean;
                ModelController.Model.SomeInt += 1;
                ModelController.Model.SomeString = DateTime.Now.ToString();

                ModelController.Model.SomeComponent.SomeOtherBoolean = !ModelController.Model.SomeComponent.SomeOtherBoolean;
                ModelController.Model.SomeComponent.SomeOtherInt += 1;
                ModelController.Model.SomeComponent.SomeOtherString = DateTime.Now.ToString();

                ModelController.Model.StillAnotherComponent.StillAnotherBoolean = !ModelController.Model.StillAnotherComponent.StillAnotherBoolean;
                ModelController.Model.StillAnotherComponent.StillAnotherInt += 1;
                ModelController.Model.StillAnotherComponent.StillAnotherString = DateTime.Now.ToString();
                //TODO:implement custom messages
                UpdateStatusBarMessages(null, null, DateTime.Now.ToLongTimeString());

                ModelController.Model.Refresh();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
            finally
            {
                StopProgressBar("Did something.");
            }
        }

        //override base
        public override void EditPreferences()
        {
            String errorMessage = null;
            FileDialogInfo fileDialogInfo = null;
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "Preferences" + ACTION_IN_PROGRESS,
                    null,
                    (Bitmap)_actionIconImages["Preferences"],
                    true,
                    33
                );

                //get folder here
                fileDialogInfo = new FileDialogInfo
                (
                    /*parent:*/ View,
                    /*modal:*/ true,
                    /*title:*/ "Select Folder...",
                    /*response:*/ DialogResult.None
                );
                
                if (!Dialogs.GetFolderPath(ref fileDialogInfo, ref errorMessage))
                {
                    throw new ApplicationException(String.Format("GetFolderPath: {0}", errorMessage));
                }

                if (fileDialogInfo.Response != DialogResult.None)
                {
                    if (fileDialogInfo.Response == DialogResult.OK)
                    {
                        UpdateStatusBarMessages(StatusMessage + fileDialogInfo.Filename + ACTION_IN_PROGRESS, null);
                    }
                    else
                    {
                        //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if dialog was cancelled
                    }
                }
                else
                {
                    //treat as cancel
                    StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                    return; //if dialog was cancelled
                }

                //override base, which did this
                // if (!Preferences())
                // {
                //     throw new ApplicationException("'Preferences' error");
                // }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("Preferences failed: {0}", ex.Message));
            }
        }

        public /*override*/ void HelpAbout()
//            where TAssemblyInfo :
//            //class,
//            AssemblyInfoBase,
//            new()
        {
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;
            AssemblyInfo assemblyInfo = null;
            AboutDialogInfo aboutDialogInfo = null;
            DialogResult response = DialogResult.None;
            String errorMessage = null;

            try
            {

                StartProgressBar
                (
                    "About" + ACTION_IN_PROGRESS, 
                    null, 
                    (Bitmap)_actionIconImages["About"], 
                    true, 
                    33
                );

                //assemblyInfo NOT passed in;specifically, NOT called by override of HelpAbout()
                assemblyInfo = new AssemblyInfo();
                Debug.Assert(this.View != null);

                // use GUI mode About feature
                aboutDialogInfo = 
                    new AboutDialogInfo
                    (
                        /*parent :*/ this.View,
                        /*modal :*/ true,
                        /*title :*/ "About " + assemblyInfo.Title,
						/*response :*/ response,
						/*programName :*/ assemblyInfo.Product,
                        /*version :*/ assemblyInfo.Version,
                        /*copyright :*/ assemblyInfo.Copyright,
                        /*comments :*/ assemblyInfo.Description,
                        /*website :*/ assemblyInfo.Website,
                        //TODO:use logo from loaded resources;this code only works because this lib is run from the context of the calling app
                        /*logo :*/ null,//TODO:this path is not xplat compatible
                        /*websiteLabel :*/ assemblyInfo.WebsiteLabel,
                        /*designers :*/ assemblyInfo.Designers,
                        /*developers :*/ assemblyInfo.Developers,
                        /*documenters :*/ assemblyInfo.Documenters,
                        /*license :*/ assemblyInfo.License
                    );

                if (!Dialogs.ShowAboutDialog(ref aboutDialogInfo, ref errorMessage))
                {
                    throw new ApplicationException(errorMessage);
                }
                
                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
        }

        /// <summary>
        /// get font name
        /// </summary>
        public void GetFont()
        {
            String errorMessage = null;
            FontDialogInfo fontDialogInfo = null;
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "Font" + ACTION_IN_PROGRESS,
                    null,
                    null, //_actionIconImages["Xxx"],
                    true,
                    33
                );

                //Note:must be set or gets null ref error after dialog closes
                Font fontDescription = null;
                fontDialogInfo = new FontDialogInfo
                (
                    /*parent:*/ View,
                    /*modal:*/ true,
                    /*title:*/ "Select Font",
                    /*response:*/ DialogResult.None,
                    /*fontDescription:*/ fontDescription
                );

                if (!Dialogs.GetFont(ref fontDialogInfo, ref errorMessage))
                {
                    throw new ApplicationException(String.Format("GetFolderPath: {0}", errorMessage));
                }

                if (fontDialogInfo.Response != DialogResult.None)
                {
                    if (fontDialogInfo.Response == DialogResult.OK)
                    {
                        UpdateStatusBarMessages(StatusMessage + fontDialogInfo.FontDescription.ToString() + ACTION_IN_PROGRESS, null);
                    }
                    else
                    {
                        //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if dialog was cancelled
                    }
                }
                else
                {
                    //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if dialog was cancelled
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
        }

        /// <summary>
        /// get color RGB
        /// </summary>
        public void GetColor()
        {
            String errorMessage = null;
            ColorDialogInfo colorDialogInfo = null;
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "Color" + ACTION_IN_PROGRESS,
                    null,
                    null, //_actionIconImages["Xxx"],
                    true,
                    33
                );

                Color color = Color.FromArgb(0, 0, 0);
                colorDialogInfo = new ColorDialogInfo
                (
                    /*parent:*/ View,
                    /*modal:*/ true,
                    /*title:*/ "Select Color",
                    /*response:*/ DialogResult.None,
                    /*color:*/ color
                );
                
                if (!Dialogs.GetColor(ref colorDialogInfo, ref errorMessage))
                {
                    throw new ApplicationException(String.Format("GetFolderPath: {0}", errorMessage));
                }

                if (colorDialogInfo.Response != DialogResult.None)
                {
                    if (colorDialogInfo.Response == DialogResult.OK)
                    {
                        UpdateStatusBarMessages(StatusMessage + colorDialogInfo.Color + ACTION_IN_PROGRESS, null);
                    }
                    else
                    {
                        //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if dialog was cancelled
                    }
                }
                else
                {
                    //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if dialog was cancelled
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
        }
        #endregion Methods

    }
}
