﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using System.Reflection;
using Ssepan.Utility;

namespace MvcForms.WinForm
{
	public class Program :
        ProgramBase //made ProgramBase for similar Program classes
    {
        #region Methods
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// <param name="args"></param>
        /// <returns>Int32</returns>
        [STAThread]
		public static Int32 Main(string[] args)
        {
            //default to fail code
            Int32 returnValue = -1;

            try
            {
				MVCView mvcView = new MVCView();
				Application.Run(mvcView);

                //return success code
                returnValue = 0;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
//				Console.Error.WriteLine(ex.Message);
            }
            finally
            {
            }
            return returnValue;
        }
        #endregion Methods
    }
}
