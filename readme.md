# readme.md - README for MvcForms.WinForm v0.8

## About

Desktop GUI app demo, on Linux(/Win/Mac), in C# / WinForms; requires ssepan.utility, ssepan.io, ssepan.application, ssepan.ui.winform, mvclibrary.
MvcForms.WinForm back-ported to Visual C# 2003 and .Net 1.1. Note: Going from later version requires giving up several features.
~ No Action type; used delegates
~ No generics; used overloads
~ No LINQ; used for and if/else
~ Different types for menu and toolbar; used ImageList for toolbar images
~ No 'default' keyword; used explicit nulls, enums, or values

![MvcForms.WinForm.png](./MvcForms.WinForm.png?raw=true "Screenshot")

### Notes

### Usage notes

~This application uses keys in app.config to identify a settings file to automatically load, for manual processing and saving. 1) The file name is given, and the format given must be one of two that the app library Ssepan.Application knows how to serialize / deserialize:  xml. 2) The value used must correspond to the format expected by the SettingsController in Ssepan.Application. It is specified with the SerializeAs property of type SerializationFormat enum. 3) The file must reside in the user's personal directory, which on Linux is /home/username. A sample file can be found in the MvcLibrary project in the Sample folder.

### VSCode

To nest files associated with forms, use the setting 'Features/Explorer/File Nesting: Patterns':
Item="*.cs", Value="${basename}.resx,${basename}.Designer.cs"

### Issues

~

### History

0.8:
~initial release

Steve Sepan
ssepanus@yahoo.com
9/29/2022
